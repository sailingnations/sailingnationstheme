<?php
add_filter('show_admin_bar', '__return_false');
//  hide warning "Warning: no-category-base-wpml.php on line 105 Creating default object from empty value "
error_reporting(E_Error);

if(is_admin()) {
	include 'includes/custom-meta-boxes.php';
	include 'includes/tour-import.php';

	include 'includes/post-types.php';
	
	add_action('admin_menu', 'my_order_menu');

	function my_order_menu() {
		add_menu_page('', 'Orders listing', 'manage_options', '#orders-listing', '');
		add_submenu_page( '#orders-listing', 'Page title', 'Personal Info', 'manage_options', '../listing-of-orders-1.php', '');
		add_submenu_page( '#orders-listing', 'Page title', 'Questionnaire Info', 'manage_options', '../listing-of-orders-2.php', '');
	}

}

include 'includes/avatars.php';
include 'includes/aq_resizer.php';
include 'includes/helper.php';
include 'includes/ajax.php';

add_filter('stylesheet_uri', 'use_parent_theme_stylesheet');
function use_parent_theme_stylesheet()
{
	//return get_template_directory_uri() . '/style.css?'.filemtime(get_template_directory().'/style.css');
	return get_stylesheet_directory_uri() . '/style.css?'.filemtime(get_stylesheet_directory().'/style.css');
}

function sailingnations_setup() {

	if (!current_user_can('administrator') && !is_admin()) {
		show_admin_bar(false);
	}

}

add_action( 'after_setup_theme', 'sailingnations_setup' );

function sn_scripts() {

	wp_dequeue_script('tourmaster-script');
	wp_enqueue_script( 'tourmaster-custom-script', get_stylesheet_directory_uri() . '/js/tourmaster.js?'.filemtime(get_stylesheet_directory().'/js/tourmaster.js'), array( 'jquery' ), 1, true );

	global $wp_locale;
	$aryArgs = array(
		'closeText'         => esc_html__('Done', 'tourmaster'),
		'currentText'       => esc_html__('Today', 'tourmaster'),
		'monthNames'        => tourmaster_strip_array_indices($wp_locale->month),
		'monthNamesShort'   => tourmaster_strip_array_indices($wp_locale->month_abbrev),
		'dayNames'          => tourmaster_strip_array_indices($wp_locale->weekday),
		'dayNamesShort'     => tourmaster_strip_array_indices($wp_locale->weekday_abbrev),
		'dayNamesMin'       => tourmaster_strip_array_indices($wp_locale->weekday_initial),
		'firstDay'          => get_option('start_of_week')
	);
	wp_localize_script( 'tourmaster-custom-script', 'TMi18n', $aryArgs );

    wp_enqueue_style('pg_style_main', get_stylesheet_directory_uri() . '/css/advanced.css');

    wp_enqueue_script( 'sailingnations-script', get_stylesheet_directory_uri() . '/js/sailingnations.js?'.filemtime(get_stylesheet_directory().'/js/sailingnations.js'), array( 'jquery' ), 1, true );
}
add_action( 'wp_enqueue_scripts', 'sn_scripts' );

	function head_google_tag() { ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PBPDH3');</script>
<!-- End Google Tag Manager -->
<?	}
	add_action( 'wp_head', 'head_google_tag' );

	add_action( 'wp_footer', 'footer_google_tag');
	function footer_google_tag() {
?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PBPDH3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php
	}

	add_filter('tourmaster_custom_post_slug', 'custom_tourmaster_post_slug', 11, 1);
	function custom_tourmaster_post_slug($param) {
		if($param === 'tour-tag') {
			return 't';
		}
		if($param === 'tour-category') {
			return 'tours';
		}

		return $param;
	}

	add_filter('tourmaster_tour_options', 'custom_tourmaster_tour_options', 11, 1);
	function custom_tourmaster_tour_options($opt) {

		if(isset($opt['tour-settings']['options'])) {
			
			$tmp = [];

			foreach($opt['tour-settings']['options'] as $k => $v) {
				$tmp[$k] = $v;
				if($k === 'tour-price-text') {
					$tmp['fixed-discount'] = [
						'title' => 'Fixed Discount',
						'type' => 'text',
						'default' => '0',
						'description' => 'The exact amount to be discounted from the tour prices.'
					];

					$tmp['tour-expired'] = [
						'title' => 'Tour Expired',
						'type' => 'combobox',
						'options' => array(
							'0' => esc_html__('No', 'tourmaster'),
							'1' => esc_html__('Yes', 'tourmaster'),
						),
						'value' => '0',
					];

					$tmp['tour-operator'] = [
						'title' => 'Tour Operator',
						'type' => 'text',
					];
					
					$templates = new WP_Query(array(
						'post_type' => 'extra_options',
						'post_status' => 'publish',
						'numberposts' => -1,
						'posts_per_page' => -1,
					));

					$available_templates = array();
					if(sizeof($templates->posts) > 0) {

						foreach ($templates->posts as $post) {
							$available_templates[(string)$post->ID] = get_the_title($post->ID);
						}
					}

					$tmp['extra_options'] = array(
						'title' => esc_html__('Extra Options', 'tourmaster'),
						'type' => 'multi-combobox',
						'options' => $available_templates,
					);
					
				}
			}
			$opt['tour-settings']['options'] = $tmp;
		}

		$opt['date-price']['options']['date-price']['options']['adult-price']['title'] = 'Male';
		$opt['date-price']['options']['date-price']['options']['children-price']['title'] = 'Female';
		unset($opt['date-price']['options']['date-price']['options']['student-price']);
		unset($opt['date-price']['options']['date-price']['options']['infant-price']);

		$opt['date-price']['options']['date-price']['options']['additional-adult']['title'] = 'Additional Male';
		$opt['date-price']['options']['date-price']['options']['additional-children']['title'] = 'Additional Female';
		unset($opt['date-price']['options']['date-price']['options']['additional-student']);
		unset($opt['date-price']['options']['date-price']['options']['additional-infant']);

		$templates = new WP_Query(array(
			'post_type' => 'yacht_templates',
			'post_status' => 'publish',
			'numberposts' => -1,
			'posts_per_page' => -1,
		));

		$available_templates = array('0' => esc_html__('None (manual entry)', 'tourmaster'));
		if(sizeof($templates->posts) > 0) {

			foreach ($templates->posts as $post) {
				$available_templates[(string)$post->ID] = get_the_title($post->ID);
			}
		}

		$opt['date-price']['options']['date-price']['options']['template'] = array(
			'title' => esc_html__('Yacht Template', 'tourmaster'),
			'type' => 'combobox',
			'options' => $available_templates,
			'condition' => array('pricing-room-base' => 'enable'),
		);


		$opt['date-price']['options']['date-price']['options']['num-rooms'] = array(
			'title' => esc_html__('Number of cabins', 'tourmaster'),
			'type' => 'combobox',
			'options' => array(
				'1' => esc_html__('1 cabin', 'tourmaster'),
				'2' => esc_html__('2 cabins', 'tourmaster'),
				'3' => esc_html__('3 cabins', 'tourmaster'),
				'4' => esc_html__('4 cabins', 'tourmaster'),
				'5' => esc_html__('5 cabins', 'tourmaster'),
			),
			'value' => '5',
			'condition' => array('pricing-room-base' => 'enable', 'template' => '0'),
		);

		$opt['date-price']['options']['date-price']['options']['per-cabin-pricing'] = array(
			'title' => esc_html__('Per-Cabin Pricing', 'tourmaster'),
			'type' => 'combobox',
			'options' => array(
				'0' => esc_html__('No', 'tourmaster'),
				'1' => esc_html__('Yes', 'tourmaster'),
			),
			'value' => '0',
			'condition' => array('pricing-room-base' => 'enable', 'template' => '0'),
		);

		for($i = 1; $i <= 5; $i++) {
			$range = array();
			for($j = $i; $j <= 5; $j++)
				$range[] = (string)$j;

			$opt['date-price']['options']['date-price']['options']["room$i-name"] =
			array(
				'title' => esc_html__("Cabin $i Name", 'tourmaster'),
				'type' => 'text',
				'condition' => array('pricing-room-base' => 'enable'/*, 'pricing-method' => 'variable'*/, 'num-rooms' => $range, 'template' => '0'),
			);
			$opt['date-price']['options']['date-price']['options']["room$i-price"] =
			array(
				'title' => esc_html__("Cabin $i Price", 'tourmaster'),
				'type' => 'text',
				'condition' => array('pricing-room-base' => 'enable'/*, 'pricing-method' => 'variable'*/, 'num-rooms' => $range, 'template' => '0')
			);
			$opt['date-price']['options']['date-price']['options']["room$i-spots"] =
			array(
				'title' => esc_html__("Cabin $i Spots", 'tourmaster'),
				'type' => 'text',
				'condition' => array('pricing-room-base' => 'enable'/*, 'pricing-method' => 'variable'*/, 'num-rooms' => $range, 'template' => '0')
			);
			$opt['date-price']['options']['date-price']['options']["room$i-description"] =
			array(
				'title' => esc_html__("Cabin $i Description", 'tourmaster'),
				'type' => 'textarea',
				'condition' => array('pricing-room-base' => 'enable'/*, 'pricing-method' => 'variable'*/, 'num-rooms' => $range, 'template' => '0')
			);
		}
										
		return $opt;
	}

	function debugging() { // debugging enabled for IP
		return in_array($_SERVER['REMOTE_ADDR'], ['31.185.123.53', '37.201.102.165']);
	}

	function getOccupiedCabins($booking_detail, $date_price) { // custom
		$a = array();
		if(!isset($booking_detail['tour-male']))
			return $a;
		
		$template = $date_price['template'];

		if($template) {
			$cabins = get_post_meta($template, '_yacht_cabins', true);
			$per_cabin_pricing = (bool)get_post_meta($template, '_yacht_per_cabin_pricing', true);
			$date_price['num-rooms'] = count($cabins);
		}
		else {
			$per_cabin_pricing = (bool)$date_price['per-cabin-pricing'];
		}


		foreach($booking_detail['tour-male'] as $cabin_index => $num_passengers) {

			if($template) {
				$cabin_spots = $cabins[$cabin_index]['spots'];
				$cabin_name = $cabins[$cabin_index]['name'];
				$cabin_price = $cabins[$cabin_index]['price'];
				$cabin_descr = $cabins[$cabin_index]['descr'];
			}
			elseif(isset($date_price['room'.($cabin_index+1).'-spots'])) {
				$cabin_spots = $date_price['room'.($cabin_index+1).'-spots'];
				$cabin_name = $date_price['room'.($cabin_index+1).'-name'];
				$cabin_price = $date_price['room'.($cabin_index+1).'-price'];
				$cabin_descr = $date_price['room'.($cabin_index+1).'-descr'];
			}


			$male_passengers = $num_passengers;
			$female_passengers = $booking_detail['tour-female'][$cabin_index];
			if(!empty($male_passengers) || !empty($female_passengers)) {
				$a[] = array(
					'cabin_index' => $cabin_index,
					'cabin_name' => $cabin_name,
					'cabin_price' => $cabin_price,
					'cabin_descr' => $cabin_descr,
					'male' => (int)$male_passengers,
					'female' => (int)$female_passengers,
					'per_cabin_pricing' => $per_cabin_pricing,
					'cabin_spots' => $cabin_spots
				);
			}
		}
		return $a;
	}

	add_filter('tourmaster_user_content_template', 'custom_tourmaster_user_content_template', 10, 2);
	function custom_tourmaster_user_content_template($template, $page_type) {
		if($page_type === 'edit-profile') {
			return get_stylesheet_directory()."/tourmaster/single/user/$page_type.php";
		}			
		return $template;
	}
	
	
	
	
	
	
	
function get_image_sizes( $size = '' ) {

	global $_wp_additional_image_sizes;

	$sizes = array();
	$get_intermediate_image_sizes = get_intermediate_image_sizes();

	// Create the full array with sizes and crop info
	foreach( $get_intermediate_image_sizes as $_size ) {

		if ( in_array( $_size, array( 'thumbnail', 'medium', 'large' ) ) ) {

			$sizes[ $_size ]['width'] = get_option( $_size . '_size_w' );
			$sizes[ $_size ]['height'] = get_option( $_size . '_size_h' );
			$sizes[ $_size ]['crop'] = (bool) get_option( $_size . '_crop' );

		} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {

			$sizes[ $_size ] = array(
				'width' => $_wp_additional_image_sizes[ $_size ]['width'],
				'height' => $_wp_additional_image_sizes[ $_size ]['height'],
				'crop' =>  $_wp_additional_image_sizes[ $_size ]['crop']
			);

		}

	}

	// Get only 1 size if found
	if ( $size ) {

		if( isset( $sizes[ $size ] ) ) {
			return $sizes[ $size ];
		} else {
			return false;
		}

	}

	return $sizes;
}

add_action('wp_ajax_upload_user_profile_image','upload_user_profile_image_call');
add_action('wp_ajax_nopriv_upload_user_profile_image','upload_user_profile_image_call');
function upload_user_profile_image_call() {
	ob_start('ob_gzhandler');
	$messages = array();
	$result = array();

	if ( 0 < $_FILES['file']['error'] ) {
		$messages[] = 'Error: ' . $_FILES['file']['error'];
		$result['status'] = false;
	}
	else {
		$available_types = array('image/jpeg','image/png');

		if(in_array($_FILES['file']['type'],$available_types)) {
			$current_user = wp_get_current_user();
			$user_id = $current_user->ID;

			$upload_dir = wp_upload_dir();

			$user_avatar_path = $upload_dir['basedir'] . '/avatars/' . $user_id;
			$user_avatar_path_with_url = $upload_dir['baseurl'] . '/avatars/' . $user_id;

			if (!file_exists($user_avatar_path)) {
				mkdir($user_avatar_path, 0777, true);
			}
			else {
				foreach(glob($user_avatar_path.'/*', GLOB_NOSORT) as $file)
					unlink($file);
			}

			$file_name = time() . '.' . pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
			$file = $user_avatar_path . '/' . $file_name;
			$file_with_url = $user_avatar_path_with_url . '/' . $file_name;

			move_uploaded_file($_FILES['file']['tmp_name'], $file);
			update_user_meta( $user_id, 'user_meta_image', $file_with_url );

			$thumb_sizes = get_image_sizes('medium');
			$avatar_img = aq_resize($file_with_url,$thumb_sizes['width'],$thumb_sizes['height'],true,true,true);

			$result['image'] = array(
				'url' => $avatar_img
			);
			$result['status'] = true;
		} else {
			$messages[] = _x('Error: Your file type is not supported','sailingnations');
			$result['status'] = false;
		}
	}

	$result['messages'] = $messages;

	die(json_encode($result));
}

add_action('wp_ajax_delete_user_profile_image','delete_user_profile_image_call');
function delete_user_profile_image_call() {
	ob_start('ob_gzhandler');

    $result = array();

	$current_user = wp_get_current_user();
	$user_id = $current_user->ID;
	
	if($image = get_user_meta($user_id, 'user_meta_image', true)) {
		
		$upload_dir = wp_upload_dir();

		$user_avatar_path = $upload_dir['basedir'] . '/avatars/' . $user_id;

		if (is_dir($user_avatar_path)) {
			foreach(glob($user_avatar_path.'/*', GLOB_NOSORT) as $file)
				unlink($file);
		}
		
		update_user_meta( $user_id, 'user_meta_image', '' );

		$gender = get_user_meta($user_id, 'customer_gender', true);

		$result['image_url'] = get_stylesheet_directory_uri().'/images/avatar_'.($gender == 1 ? 'male' : 'female').'.png';

		$result['status'] = true;
	}
	else {
		$result['status'] = false;
		$result['messages'] = 'There was no profile image to remove.';
	}

    die(json_encode($result));
}



function modify_query_order_tag( $query ) {
	
	if ( $query->is_archive() && ($query->is_tax('tour_category') || $query->is_tax('tour_tag')) && $query->is_main_query() && !is_admin() ) {
		$query->set( 'orderby', ['meta_value_num' => 'ASC', 'title' => 'ASC' ] );
		$query->set( 'meta_key', 'tour-next-date' );
//		$query->set( 'order', 'ASC' );
		// $meta_query = $query->get('meta_query');
		// $meta_query[] = array(
		// 	'key' => 'tourmaster-tour-option',
		// 	'compare' => 'NOT LIKE',
		// 	'value' => '"tour-expired";s:1:"1"'
		// );

		// $query->set('meta_query',$meta_query);
	}

}
add_action( 'pre_get_posts', 'modify_query_order_tag' );


add_action('init', 'custom_rewrite_basic');
function custom_rewrite_basic() {
	add_rewrite_tag('%location%','([^&]+)');
	add_rewrite_tag('%duration%','([^&]+)');
	add_rewrite_tag('%date%','([^&]+)');
	add_rewrite_tag('%sort%','([^&]+)');
	add_rewrite_tag('%tour-search%','([^&]+)');
	add_rewrite_tag('%seo%','([^&]+)');
	add_rewrite_tag('%paged%','([^&]+)');
	add_rewrite_tag('%user-tour-date%','([^&]+)');
	add_rewrite_tag('%user-tour-date-long%','([^&]+)');


	if(ICL_LANGUAGE_CODE == 'de') {
		
		add_rewrite_rule(
			'^sailing/([^/]+)/([^/]+)/([^/]+)/([^/]+)/?',
			'index.php?page_id=19609&tour-search=1&location=$matches[1]&duration=$matches[3]&date=$matches[2]&sort=$matches[4]',
			'top'
		);
		add_rewrite_rule(
			'^sailing/([^/]+)/([^/]+)/([^/]+)/([^/]+)/page/(\d+)/?',
			'index.php?page_id=19609&tour-search=1&location=$matches[1]&duration=$matches[3]&date=$matches[2]&sort=$matches[4]&paged=$matches[5]',
			'top'
		);
	}
	else {
		add_rewrite_rule(
			'^sailing/([^/]+)/([^/]+)/([^/]+)/([^/]+)/?$',
			'index.php?pagename=search-tours-sailing-holidays&tour-search=1&location=$matches[1]&duration=$matches[3]&date=$matches[2]&sort=$matches[4]',
			'top'
		);
		add_rewrite_rule(
			'^sailing/([^/]+)/([^/]+)/([^/]+)/([^/]+)/page/(\d+)/?$',
			'index.php?pagename=search-tours-sailing-holidays&tour-search=1&location=$matches[1]&duration=$matches[3]&date=$matches[2]&sort=$matches[4]&paged=$matches[5]',
			'top'
		);
	}
	
	
	add_rewrite_rule(
		'^tour/([^/]+)/(\d{4}-\d{2}-\d{2})/?$',
		'index.php?tour=$matches[1]&user-tour-date=$matches[2]',
		'top'
	);
	add_rewrite_rule(
		'^tour/([^/]+)/(\d{4}-[A-Za-z]{3,10}-\d{2})/?$',
		'index.php?tour=$matches[1]&user-tour-date-long=$matches[2]',
		'top'
	);

	flush_rewrite_rules();

}


/*
* Yoast SEO Disable Automatic Redirects for
* Posts And Pages
* Credit: Yoast Development Team
* Last Tested: May 09 2017 using Yoast SEO Premium 4.7.1 on WordPress 4.7.4
*/

add_filter('wpseo_premium_post_redirect_slug_change', '__return_true' );

add_filter('template_include', 'sailin_tourmaster_template_registration',99999, 1);
function sailin_tourmaster_template_registration( $template ) {
	$template_name_parts = explode('/',$template);
	$plugin_dir = ABSPATH . 'wp-content/plugins/';
	$template_file_name = str_replace($plugin_dir,'',$template);

	$theme_path = get_stylesheet_directory() . '/' . $template_file_name;

	if(file_exists($theme_path)) {
		$template = $theme_path;
		//die;
	}

	//echo $theme_path;

	return $template;
}

function sailin_registration_form($html) {
	require_once('includes/recaptcha/recaptchalib.php');
	$keys = get_googlecaptcha_keys();
	
	//$key = '6LfrnA4TAAAAALs_MQQLNE207hUXFQ8DbpjuTcPY';
	//$secret = '6LfrnA4TAAAAAK5xaYkoSpSbogtVVZKLoFDRArvv';
  
	$html = str_replace('</div><input type="hidden" name="redirect"','<div class="tourmaster-profile-field clearfix">'.recaptcha_get_html($keys['key']).'</div></div><input type="hidden" name="redirect"',$html);
	echo $html;
}

function get_googlecaptcha_keys() {
	$cf7_data = get_option( 'wpcf7' );
	$keys = array();

	if(sizeof($cf7_data['recaptcha']) > 0) {
		$site_keys = array_keys($cf7_data['recaptcha']);
		$secret = $cf7_data['recaptcha'][$site_keys[0]];
		
		$keys['key'] = $site_keys[0];
		$keys['secret'] = $secret;
	}

	return $keys;
}

add_action('tourmaster_register_form_after_fields','sailin_tourmaster_register_form_after_fields',10);
function sailin_tourmaster_register_form_after_fields() {
	require_once('includes/recaptcha/recaptchalib.php');
	$keys = get_googlecaptcha_keys();

	echo '<div class="tourmaster-profile-field tourmaster-profile-field-captcha clearfix"><div id="tour-captcha"></div><div class="g-recaptcha" data-sitekey="'.$keys['key'].'"></div></div>';
}

function sailin_captcha_verify( $response_token ) {
	$keys = get_googlecaptcha_keys();
	$is_human = false;

	if ( empty( $response_token ) ) {
		return $is_human;
	}

	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$sitekey = $keys['key'];
	$secret = $keys['secret'];

	$response = wp_safe_remote_post( $url, array(
		'body' => array(
			'secret' => $secret,
			'response' => $response_token,
			'remoteip' => $_SERVER['REMOTE_ADDR'],
		),
	) );

	if ( 200 != wp_remote_retrieve_response_code( $response ) ) {
		return $is_human;
	}

	$response = wp_remote_retrieve_body( $response );
	$response = json_decode( $response, true );

	$is_human = isset( $response['success'] ) && true == $response['success'];
	return $is_human;
}

// Add scripts to wp_head()
function child_theme_head_script() { ?>
	<?php $keys = get_googlecaptcha_keys(); ?>
	<script>
		jQuery(window).on('load', function($) {
			jQuery(document).on('click','[data-tmlb="signup"]', function() {
				initGoogleCaptcha();
			});

			if(jQuery('body').hasClass('tourmaster-template-register')) {
				initGoogleCaptcha();
			}

			function initGoogleCaptcha() {
				jQuery.getScript('https://www.google.com/recaptcha/api.js', function() {
					grecaptcha.render('tour-captcha', {
					'sitekey' : '<?php echo $keys['key']; ?>'
					});
				});
			}

		});

	</script>
<?php
    wp_register_style( 'header-mobile', get_template_directory() .'/css/header-mobile.css' );
    wp_enqueue_style( 'header-mobile' );
}
add_action( 'wp_head', 'child_theme_head_script' );


//////////
/// debug
///
///////////
function _error_log ( $log = '------DEBUG INFO------' )  {
//    if ( true !== WP_DEBUG ) {
//        return;
//    }
    $place = get_stylesheet_directory() . '/errors.log';

    error_log( PHP_EOL . '[' . date('Y-m-d H:m:i') . ']' . '   //////////----------START-----------////////' . PHP_EOL, 3, $place);

    $trace = debug_backtrace();
    if (!empty($trace[0])) {
        error_log('[FILE :' . $trace[0]['file'] . '][LINE :' . $trace[0]['line'] . ']' . PHP_EOL, 3, $place);
    }

    if ( is_array( $log ) || is_object( $log ) ) {
        error_log( print_r( $log, true ) . PHP_EOL, 3, $place );
    } else {
        error_log( $log . PHP_EOL, 3, $place );
    }

}
