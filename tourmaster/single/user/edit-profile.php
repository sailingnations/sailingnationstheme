<?php
	$profile_fields = tourmaster_get_profile_fields();

	echo '<div class="tourmaster-user-content-inner tourmaster-user-content-inner-edit-profile" >';
	tourmaster_get_user_breadcrumb();

	// update data
	if( isset($_POST['tourmaster-edit-profile']) ){
		$verify = tourmaster_validate_profile_field($profile_fields);

		if( is_wp_error($verify) ){
			$error_messages = '';
			foreach( $verify->get_error_messages() as $messages ){
				$error_messages .= empty($error_messages)? '': '<br />';
				$error_messages .= $messages;
			}
			tourmaster_user_update_notification($error_messages, false);
			
			foreach( $profile_fields as $slug => $field ){
				if( $slug == 'email' ) continue;

				if(!empty($_POST[$slug])) {
					$_POST[$slug] = stripslashes($_POST[$slug]);
				}
			}
		}else{
			tourmaster_update_profile_field($profile_fields);
			tourmaster_user_update_notification(esc_html__('Your profile has been successfully changed.', 'tourmaster'));
		}
	}

	// edit profile page content
	echo '<form class="tourmaster-edit-profile-wrap tourmaster-form-field" method="POST" >';

	echo '<div class="tourmaster-edit-profile-avatar" >';
	echo get_avatar($current_user->data->ID, 85);
	?>
	<input type="hidden" name="user_meta_image" id="user_meta_image" value="<?php echo esc_url_raw( get_the_author_meta( 'user_meta_image', $user_id ) ); ?>" class="regular-text" />
	<!-- Outputs the save button -->
	<input class="hidden_field" style="display: none;" type="file" accept="image/*" name="user_avatar" id="user_avatar">
<?
	echo '<input type="submit" class="tourmaster-button" id="user-avatar-change" value="' . esc_html__('Change Profile Picture', 'tourmaster') . '">';
	echo '</div>';

	foreach( $profile_fields as $slug => $profile_field ){
		$profile_field['slug'] = $slug;
		tourmaster_get_form_field($profile_field, 'profile');
	}

	echo '<input type="submit" class="tourmaster-edit-profile-submit tourmaster-button" value="' . esc_html__('Update Profile', 'tourmaster') . '" />';
	echo '<input type="hidden" name="tourmaster-edit-profile" value="1" />';
	echo '</form>'; // tourmaster-edit-profile-wrap

	echo '</div>'; // tourmaster-user-content-inner
?>
<script>
var $ = jQuery;
$('#user_avatar').on('change', function () {
	var form = $(this).closest('form');
	if(form.hasClass('saving') || !$(this).prop('files')[0])
		return;
	var file_data = $(this).prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('action', 'upload_user_profile_image');

	$.ajax({
		url: '/wp-admin/admin-ajax.php',
		dataType: 'json',
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,
		method: 'post',
		beforeSend: function() {
			$('#user-avatar-change').fadeOut("fast");
		},
		success: function(result) {
			if(result.status) {
				$('.tourmaster-edit-profile-avatar img').attr('src',result.image.url).show();
			} else {
				alert(result.messages);
			}
			$('#user-avatar-change').fadeIn("fast");
		}
	});
});
	
$('#user-avatar-change').click(function(event) {
	$('#user_avatar').click();
	event.preventDefault();
});

</script>