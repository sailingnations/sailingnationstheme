<?php

//if($_SERVER['REMOTE_ADDR']!=='31.185.123.53') {
	if(!get_query_var('tour-search')) {

	
		$url = "/sailing";
		if ( defined( 'ICL_LANGUAGE_CODE' ) && ICL_LANGUAGE_CODE !== 'en' ) {
			$url = '/'. ICL_LANGUAGE_CODE . $url;
		}
		foreach(['location','date','duration','sort'] as $param) {
			if($qry = get_query_var($param)) {
				if($param === 'date') {
					$parts = explode('-', $qry);
					if(count($parts) !== 2) {
						$url .= '/-';
						continue;
					}

					$dt = DateTime::createFromFormat('!m', (int)$parts[1]);
					$month = strtolower($dt->format('F'));

					$url .= '/'.$month.'-'.$parts[0];
				}
				else
					$url .= '/'.$qry;
			}
			else
				$url .= '/-';
		}

		$url .= '/';
		header('Location: '.$url);
		exit;
	}
	else {
		foreach(['location','date','duration','sort','paged'] as $param) {
			$qry = get_query_var($param);
			if($qry !== '-') {
				if($param === 'date') {
					$parts = explode('-', $qry);
					$date = date_parse($parts[0]);
					$month = (int)$date['month'];
					if($month < 10)
						$month = '0' . $month;

					$_GET[$param] = $parts[1].'-'.$month;
				}
				else
					$_GET[$param] = $qry;
			}
		}

	}
//}


get_header();

do_action('gdlr_core_print_page_builder');
	$settings = array(
		'pagination' => 'page',
		'tour-style' => tourmaster_get_option('general', 'search-page-tour-style', 'full'),
		'column-size' => tourmaster_get_option('general', 'search-page-column-size', '20'),
		'thumbnail-size' => tourmaster_get_option('general', 'search-page-thumbnail-size', 'full'),
		'tour-info' => tourmaster_get_option('general', 'search-page-tour-info', array()),
		'excerpt' => tourmaster_get_option('general', 'search-page-with-excerpt', 'specify-number'),
		'excerpt-number' => tourmaster_get_option('general', 'search-page-excerpt-number', '55'),
		'tour-rating' => tourmaster_get_option('general', 'search-page-tour-rating', 'enable'),
		'custom-pagination' => true
	);
	$settings['paged'] = (get_query_var('paged'))? get_query_var('paged') : get_query_var('page');
	$settings['paged'] = empty($settings['paged'])? 1: $settings['paged'];

	// search query
	$args = array(
		'post_status' => 'publish',
		'post_type' => 'tour',
		'posts_per_page' => tourmaster_get_option('general', 'search-page-num-fetch', '9'),
		'paged' => $settings['paged'],
	);

	//if(

	// keywords
	if( !empty($_GET['tour-search']) ){
		$args['s'] = $_GET['tour-search'];
	}

	// location
	if( !empty($_GET['location']) ){
		$args['tax_query'] = array(
			array('terms'=>$_GET['location'], 'taxonomy'=>'tour_category', 'field'=>'slug')
		);
	}

	$meta_query = array();
	$meta_query[] = array(
		'key' => 'tourmaster-tour-option',
		'compare' => 'NOT LIKE',
		'value' => '"tour-expired";s:1:"1"'
	);
	// duration
	if( !empty($_GET['duration']) ){
		if( $_GET['duration'] == '1-day' ){
			$meta_query[] = array(
				'key'     => 'tourmaster-tour-duration',
				'value'   => '1',
				'compare' => '=',
			);
		}else if( $_GET['duration'] == '2-5-days' ){
			$meta_query[] = array(
				'key'     => 'tourmaster-tour-duration',
				'value'   => array(2, 5),
				'compare' => 'BETWEEN',
				'type'    => 'NUMERIC'
			);
		}else if( $_GET['duration'] == '5-8-days' ){
			$meta_query[] = array(
				'key'     => 'tourmaster-tour-duration',
				'value'   => array(5, 8),
				'compare' => 'BETWEEN',
				'type'    => 'NUMERIC'
			);
		}else if( $_GET['duration'] == '8-days' ){
			$meta_query[] = array(
				'key'     => 'tourmaster-tour-duration',
				'value'   => '8',
				'compare' => '>'
			);
		}
	}

	// date
	if( !empty($_GET['date']) ){
		$yearmonth = (string)$_GET['date'];

		$meta_query[] = array(
			'key'     => 'tourmaster-tour-date',
			'value'   => $yearmonth.'-',
			'compare' => 'LIKE',
		);
	}	

	// min price 
	if( !empty($_GET['min-price']) ){
		$meta_query[] = array(
			'key'     => 'tourmaster-tour-price',
			'value'   => $_GET['min-price'],
			'compare' => '>=',
			'type'    => 'DECIMAL'
		);
	}

	// max price 
	if( !empty($_GET['max-price']) ){
		$meta_query[] = array(
			'key'     => 'tourmaster-tour-price',
			'value'   => $_GET['max-price'],
			'compare' => '<=',
			'type'    => 'DECIMAL'
		);
	}

	// sort
	if( !empty($_GET['sort']) ){
		$sort = $_GET['sort'];
		$criteria = array(
			'highest-price' => ['tourmaster-tour-price', 'DESC'],
			'lowest-price' => ['tourmaster-tour-price', 'ASC'],
		);
		if(isset($criteria[$sort])) {
			$args['orderby'] = 'meta_value_num';
			$args['meta_key'] = $criteria[$sort][0];
			$args['order'] = $criteria[$sort][1];
		}
	}
	else {
		$args['orderby'] = ['meta_value_num' => 'ASC', 'title' => 'ASC'];
		$args['meta_key'] = 'tour-next-date';
		//$args['order'] = 'ASC';
	}

	if( !empty($meta_query) ){
		$args['meta_query'] = $meta_query;
	}
	$settings['query'] = new WP_Query($args);

	// start the content
	echo '<div class="tourmaster-template-wrapper" >';
	echo '<div class="tourmaster-container" >';

	// sidebar content
	$sidebar_type = tourmaster_get_option('general', 'search-sidebar', 'none');
	echo '<div class="' . tourmaster_get_sidebar_wrap_class($sidebar_type) . '" >';
	echo '<div class="' . tourmaster_get_sidebar_class(array('sidebar-type'=>$sidebar_type, 'section'=>'center')) . '" >';
	echo '<div class="tourmaster-page-content" >';
	
	if(!$settings['query']->post_count) { ?>
	<div class="tourmaster-item-list tourmaster-tour-medium clearfix tourmaster-tour-frame gdlr-core-skin-e-background">
		<div class="tourmaster-tour-content-wrap clearfix">
			<?=esc_html__('No tours available for this location and date. Please try a new search.', 'tourmaster')?>
		</div>
	</div>
<? } 
	else
		echo tourmaster_pb_element_tour::get_content($settings);

	echo '</div>'; // tourmaster-page-content
	echo '</div>'; // traveltour-get-sidebar-class

	// sidebar left
	if( $sidebar_type == 'left' || $sidebar_type == 'both' ){
		$sidebar_left = tourmaster_get_option('general', 'search-sidebar-left');
		echo tourmaster_get_sidebar($sidebar_type, 'left', $sidebar_left);
	}

	// sidebar right
	if( $sidebar_type == 'right' || $sidebar_type == 'both' ){
		$sidebar_right = tourmaster_get_option('general', 'search-sidebar-right');
		echo tourmaster_get_sidebar($sidebar_type, 'right', $sidebar_right);
	}

	echo '</div>'; // traveltour-get-sidebar-wrap-class	
	
	echo '</div>'; // tourmaster-container
	echo '</div>'; // tourmaster-template-wrapper

get_footer(); 

?>