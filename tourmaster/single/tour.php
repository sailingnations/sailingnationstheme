<?php
	/**
	 * The template for displaying single tour posttype
	 */

	// calculate view count before printing the content
	$view_count = get_post_meta(get_the_ID(), 'tourmaster-view-count', true);
	$view_count = empty($view_count)? 0: intval($view_count);
	if( empty($_COOKIE['tourmaster-tour-' . get_the_ID()]) ){
		$view_count = $view_count + 1;
		update_post_meta(get_the_ID(), 'tourmaster-view-count', $view_count);
		setcookie('tourmaster-tour-' . get_the_ID(), 1, time() + 86400);
	}

	if( !empty($_POST['tour_temp']) ){
		$temp_data = tourmaster_process_post_data($_POST['tour_temp']);
		$temp_data = json_decode($temp_data, true);
		unset($temp_data['tour-id']);
	}

get_header();
	echo '<div class="tourmaster-page-wrapper" id="tourmaster-page-wrapper" >';

	global $current_user;
	$tour_style = new tourmaster_tour_style();
	$tour_option = tourmaster_get_post_meta(get_the_ID(), 'tourmaster-tour-option');

	////////////////////////////////////////////////////////////////////
	// header section
	////////////////////////////////////////////////////////////////////
	if( empty($tour_option['header-image']) || $tour_option['header-image'] == 'feature-image' ){
		echo '<div class="tourmaster-single-header" ' . tourmaster_esc_style(array('background-image' => get_post_thumbnail_id())) . ' >';
	}else if( $tour_option['header-image'] == 'custom-image' && !empty($tour_option['header-image-custom']) ){
		echo '<div class="tourmaster-single-header" ' . tourmaster_esc_style(array('background-image' => $tour_option['header-image-custom'])) . ' >';
	}else if( $tour_option['header-image'] == 'slider' && !empty($tour_option['header-slider']) ){
		
		$slides = array();
		$thumbnail_size = empty($tour_option['header-slider-thumbnail'])? 'full': $tour_option['header-slider-thumbnail'];
		foreach( $tour_option['header-slider'] as $slider ){
			$slides[] = '<div class="tourmaster-media-image" >' . tourmaster_get_image($slider['id'], $thumbnail_size) . '</div>';
		}

		echo '<div class="tourmaster-single-header tourmaster-with-slider" >';
		echo gdlr_core_get_flexslider($slides, array('navigation' => 'none'));
	}else{
		echo '<div class="tourmaster-single-header" >';
	}

	echo '<div class="tourmaster-single-header-top-overlay" ></div>';
	echo '<div class="tourmaster-single-header-overlay" ></div>';
	echo '<div class="tourmaster-single-header-container tourmaster-container" >';
	echo '<div class="tourmaster-single-header-container-inner" >';
	echo '<div class="tourmaster-single-header-title-wrap tourmaster-item-pdlr" ';
	if( empty($tour_option['header-image']) || in_array($tour_option['header-image'], array('feature-image', 'custom-image')) ){
		echo tourmaster_esc_style(array(
			'padding-top' => empty($tour_option['header-top-padding'])? '': $tour_option['header-top-padding'],
			'padding-bottom' => empty($tour_option['header-bottom-padding'])? '': $tour_option['header-bottom-padding'],
		));
	}
	echo ' >';
	echo '<h1 class="tourmaster-single-header-title" >' . get_the_title() . '</h1>';
	echo $tour_style->get_rating();

	echo '</div>'; // tourmaster-single-header-title-wrap

	$header_price  = '<div class="tourmaster-header-price tourmaster-item-mglr" >';
	$header_price .= '<div class="tourmaster-header-price-ribbon" >';
	if( !empty($tour_option['promo-text']) ){
		$header_price .= __($tour_option['promo-text'], 'tourmaster-customfields');
	}else{
		$header_price .= esc_html__('Price', 'tourmaster');
	}
	$header_price .= '</div>';
	$header_price .= '<div class="tourmaster-header-price-wrap" >';
	$header_price .= '<div class="tourmaster-header-price-overlay" ></div>';
	$header_price .= $tour_style->get_price(array('with-info' => true));
	$header_price .= '</div>'; // tourmaster-header-price-wrap
	$header_price .= '</div>'; // touramster-header-price 

	echo $header_price;
	echo '</div>'; // tourmaster-single-header-container-inner
	echo '</div>'; // tourmaster-single-header-container
	echo '</div>'; // tourmaster-single-header


	////////////////////////////////////////////////////////////////////
	// content section
	////////////////////////////////////////////////////////////////////
	echo '<div class="tourmaster-template-wrapper" >';
	
	// tourmaster booking bar
	echo '<div class="tourmaster-tour-booking-bar-container tourmaster-container" >';
	echo '<div class="tourmaster-tour-booking-bar-container-inner" >';
	echo '<div class="tourmaster-tour-booking-bar-anchor tourmaster-item-mglr" ></div>';
	echo '<div class="tourmaster-tour-booking-bar-wrap tourmaster-item-mglr" >';
	echo '<div class="tourmaster-tour-booking-bar-outer" >';
	echo $header_price;

	echo '<div class="tourmaster-tour-booking-bar-inner" >';
	echo '<form class="tourmaster-single-tour-booking-fields tourmaster-form-field tourmaster-with-border" method="post" ';
	echo 'action="' . esc_url(tourmaster_get_template_url('payment')) . '" ';
	echo 'id="tourmaster-single-tour-booking-fields" data-ajax-url="' . esc_url(TOURMASTER_AJAX_URL) . '" >';

	echo '<input type="hidden" name="tour-id" value="' . esc_attr(get_the_ID()) . '" />';
	$available_date = explode(',', get_post_meta(get_the_ID(), 'tourmaster-tour-date-avail', true));
	if( !empty($available_date) ){	
		echo '<div class="tourmaster-tour-booking-date clearfix" data-step="1" >';
		echo '<i class="fa fa-calendar" ></i>';
		echo '<div class="tourmaster-tour-booking-date-input" >';

		$selected_date = $available_date[0];
		if( !empty($temp_data['tour-date']) ){
			$selected_date = $temp_data['tour-date'];
			unset($temp_data['tour-date']);
		}
		elseif( isset($_GET['tour-date'])) {
			$selected_date = $_GET['tour-date'];
		}
		elseif( get_query_var('user-tour-date') ) {
			$selected_date = get_query_var('user-tour-date');
		}
		elseif( get_query_var('user-tour-date-long') ) {
			$selected_date = get_query_var('user-tour-date-long');
			for($i = 1; $i <= 12; $i++) {
				$dt = DateTime::createFromFormat('!m', $i);
				DateTime::createFromFormat('!m', $i);
				
				$selected_date = str_replace(strtolower($dt->format('F')), strtolower($dt->format('M')), $selected_date);
			}
			$tmp = strtotime($selected_date);
			$selected_date = date('Y-m-d', $tmp);
		}

		/*if( sizeof($available_date) == 1 ){
			$return_date = strtotime($selected_date.' + '.((int)$tour_option['multiple-duration'] - 1).' days');

			echo '<div class="tourmaster-tour-booking-date-display" >' . tourmaster_date_format($selected_date) . ' &ndash; '.tourmaster_date_format($return_date).'</div>';
			echo '<input type="hidden" name="tour-date" value="' . esc_attr($selected_date) . '" />';
		}else{*/
			echo '<div class="tourmaster-datepicker-wrap" >';
			echo '<input type="text" class="tourmaster-datepicker" readonly ';
			echo 'value="' . esc_attr($selected_date) . '" ';
			echo 'data-date-format="' . esc_attr(tourmaster_get_option('general', 'datepicker-date-format', 'd M yy')) . '" ';
			echo 'data-tour-range="' . (empty($tour_option['multiple-duration'])? 1: intval($tour_option['multiple-duration'])) . '" ';
			echo 'data-tour-date="' . esc_attr(json_encode($available_date)) . '" />';
			echo '<input type="hidden" name="tour-date" class="tourmaster-datepicker-alt" />';
			echo '</div>';
			echo '<style>
			.tourmaster-body .ui-datepicker table tr td span, .tourmaster-body .ui-datepicker table tr td a
			{
				border-radius: 0;
				-webkit-border-radius: 0;
				-moz-border-radius: 0;
				width: 40px;
				height: 40px;
				line-height: 32px;
				padding: 0;
				position: relative;
				vertical-align: middle;
				
				
			}
			td.ui-datepicker-week-end a {
				line-height: 32px !important;
			}

			'.PHP_EOL;

			$tpl_ids = [];
			foreach($tour_option['date-price'] as $setting) {
				if(isset($setting['template']))
					$tpl_ids[] = $setting['template'];
			}

			$templates = new WP_Query(array(
				'post_type' => 'yacht_templates',
				'post_status' => 'publish',
				'numberposts' => -1,
				'posts_per_page' => -1,
				'post__in' => $tpl_ids
			));

			$lowest_prices = [];

			if($templates->post_count) {
				foreach($templates->posts as $tpl) {
					$cabins = get_post_meta($tpl->ID, '_yacht_cabins', true);
					$lowest_price = 0;
					foreach($cabins as $cabin) {
						if(!$lowest_price || $cabin['price'] < $lowest_price) {
							$lowest_price = $cabin['price'];
						}
					}

					foreach($tour_option['date-price'] as $setting) {
						if(isset($setting['template']) && $setting['template'] == $tpl->ID) {
							foreach(explode(',', $setting['extra-date']) as $date) {
								$date = trim($date);
								if(!isset($lowest_prices[$date]) || $lowest_price < $lowest_prices[$date]) {
									$lowest_prices[$date] = $lowest_price;
								}
							}
						}
					}
					
				}
			}

			foreach($available_date as $k => $v) {
				$eventDate = strtotime($v);

				$mm = date('n', $eventDate)-1;
				$month = date('m', $eventDate);
				$yy = date('Y', $eventDate);
				$dd = date('d', $eventDate);

				echo 'td.day_'.$dd.'[data-month="'.$mm.'"][data-year="'.$yy.'"] a:after {
					content: "€'.$lowest_prices[$yy.'-'.$month.'-'.$dd].'";
					display: block;
					position: absolute;
					color: #5b5b5b;
					top: 14px;
					font-size: 8.5pt;
					width: 40px;
					text-align: center;
				}
				td.day_'.$dd.'.tourmaster-highlight[data-month="'.$mm.'"][data-year="'.$yy.'"] a:after {
					color: #fff;
				}
				td.day_'.$dd.':not(.tourmaster-highlight)[data-month="'.$mm.'"][data-year="'.$yy.'"]:hover a:after {
					color: #fff;
				}
				'.PHP_EOL;
			}
			echo '</style>';
		
		//}
		echo '</div>';
		echo '</div>'; // tourmaster-tour-booking-date

		$booking_value = array();
		if( !empty($temp_data) ){
			$booking_value = array(
				'tour-people' => empty($temp_data['tour-people'])? '': $temp_data['tour-people'],
				'tour-room' => empty($temp_data['tour-room'])? '': $temp_data['tour-room'],
				'tour-adult' => empty($temp_data['tour-adult'])? '': $temp_data['tour-adult'],
				'tour-children' => empty($temp_data['tour-children'])? '': $temp_data['tour-children'],
				'tour-student' => empty($temp_data['tour-student'])? '': $temp_data['tour-student'],
				'tour-infant' => empty($temp_data['tour-infant'])? '': $temp_data['tour-infant'],
			);
			unset($temp_data['tour-people']);
			unset($temp_data['tour-room']);
			unset($temp_data['tour-adult']);
			unset($temp_data['tour-children']);
			unset($temp_data['tour-student']);
			unset($temp_data['tour-infant']);
		}

		echo tourmaster_get_tour_booking_fields(array(
			'tour-id' => get_the_ID(),
			'tour-date' => $selected_date
		), $booking_value);
	}else{
		echo '<div class="tourmaster-tour-booking-bar-error" data-step="999" >';
		echo esc_html__('The tour is not available yet.', 'tourmaster');
		echo '</div>';
	}

	// carry over data
	if( !empty($temp_data) ){
		foreach( $temp_data as $field_name => $field_value ){
			if( is_array($field_value) ){
				foreach( $field_value as $field_single_value ){
					echo '<input type="hidden" name="' . esc_attr($field_name) . '[]" value="' . esc_attr($field_single_value) . '" />';
				}
			}else{
				echo '<input type="hidden" name="' . esc_attr($field_name) . '" value="' . esc_attr($field_value) . '" />';
			}
		}
	}
	
	echo '</form>'; // tourmaster-tour-booking-fields

	// if not logging in print the login before proceed form
	if( !is_user_logged_in() ){
		echo tourmaster_lightbox_content(array(
			'id' => 'proceed-without-login',
			'title' => esc_html__('Proceed Booking', 'tourmaster'),
			'content' => tourmaster_get_login_form2(false, array(
				'continue-as-guest'=>false,
				'redirect'=>'payment'
			))
		));
	}

	// bottom bar for wish list and view count
	echo '<div class="tourmaster-booking-bottom clearfix" >';
	
	// wishlist section
	$logged_in = is_user_logged_in();
	if( !$logged_in ){
		echo '<div class="tourmaster-save-wish-list" data-tmlb="wish-list-login" >';
	}else{
		$wish_list = get_user_meta($current_user->ID, 'tourmaster-wish-list', true);
		$wish_list = empty($wish_list)? array(): $wish_list;
		$wish_list_active = in_array(get_the_ID(), $wish_list);

		if( !$wish_list_active ){
			echo '<div class="tourmaster-save-wish-list" ';
			echo 'id="tourmaster-save-wish-list" ';
			echo 'data-ajax-url="' . esc_url(TOURMASTER_AJAX_URL) . '" ';
			echo 'data-tour-id="' . esc_attr(get_the_ID()) . '" ';
			echo '>';
		}else{
			echo '<div class="tourmaster-save-wish-list tourmaster-active" >';
		}
	}
	echo '<span class="tourmaster-save-wish-list-icon-wrap" >';
	echo '<i class="tourmaster-icon-active fa fa-heart" ></i>';
	echo '<i class="tourmaster-icon-inactive fa fa-heart-o" ></i>';
	echo '</span>';
	echo esc_html__('Save To Wish List', 'tourmaster');
	echo '</div>'; // tourmaster-save-wish-list
	if( !$logged_in ){
		echo tourmaster_lightbox_content(array(
			'id' => 'wish-list-login',
			'title' => esc_html__('Adding item to wishlist requires an account', 'tourmaster'),
			'content' => tourmaster_get_login_form2(false)
		));
	}

	echo '<div class="tourmaster-view-count" >';
	echo '<i class="fa fa-eye" ></i>';
	echo '<span class="tourmaster-view-count-text" >' . $view_count . '</span>';
	echo '</div>'; // tourmaster-view-count
	echo '</div>'; // tourmaster-booking-bottom
	echo '</div>'; // tourmaster-tour-booking-bar-inner
	echo '</div>'; // tourmaster-tour-booking-bar-outer

	$social = tourmaster_get_tour_social(array(
		'tour-id' => get_the_ID(),
		'tour-date' => $selected_date
	));

	if(1 || debugging()) { ?>
<div class="tourmaster-tour-booking-bar-widget  traveltour-sidebar-area" style="display: <?=($social ? 'block' : 'none')?>">
<div id="text-11" class="widget widget_text traveltour-widget"> <div class="textwidget"><div class="gdlr-core-space-shortcode" style="margin-top: -25px;"></div>
<div class="gdlr-core-widget-list-shortcode" id="gdlr-core-widget-list-0"><h3 class="gdlr-core-widget-list-shortcode-title">My Crew</h3>
<style>
#tourmaster-tour-booking-people-container li {
	display: inline-block;
	width: 32%;
	padding-left: 0px;
	padding-right: 0px;
}
</style>
<ul id="tourmaster-tour-booking-people-container">
<?	echo $social;
?>
</ul>
</div></div>
</div>
</div>
<?
	}

	// sidebar widget
	if( !empty($tour_option['sidebar-widget']) && $tour_option['sidebar-widget'] != 'none' ){
		$sidebar_class = apply_filters('gdlr_core_sidebar_class', '');

		echo '<div class="tourmaster-tour-booking-bar-widget ' . esc_attr($sidebar_class) . '" >';
		if( $tour_option['sidebar-widget'] == 'default' ){
			$sidebar_name = tourmaster_get_option('general', 'single-tour-default-sidebar', 'none');
			if( $sidebar_name != 'none' && is_active_sidebar($sidebar_name) ){
				dynamic_sidebar($sidebar_name); 
			}
		}else{
			if( is_active_sidebar($tour_option['sidebar-widget']) ){ 
				dynamic_sidebar($tour_option['sidebar-widget']); 
			}
		}
		echo '</div>';
	}
	echo '</div>'; // tourmaster-tour-booking-bar-wrap
	echo '</div>'; // tourmaster-tour-booking-bar-container-inner
	echo '</div>'; // tourmaster-tour-booking-bar-container

	// print tour top info
	echo '<div class="tourmaster-tour-info-outer" >';
	echo '<div class="tourmaster-tour-info-outer-container tourmaster-container" >';
	echo $tour_style->get_info(array( 'duration-text', 'availability', 'departure-location', 'return-location', 'minimum-age', 'maximum-people', 'operator'), array(
		'info-class' => 'tourmaster-item-pdlr'
	));
	echo '</div>'; // tourmaster-tour-info-outer-container
	echo '</div>'; // tourmaster-tour-info-outer
	
	global $post;
	while( have_posts() ){ the_post();

		ob_start();
		the_content();
		$content = ob_get_contents();
		ob_end_clean();

		if( !empty($content) ){
			echo '<div class="tourmaster-container" >';
			echo '<div class="tourmaster-page-content tourmaster-item-pdlr" >';
			echo '<div class="tourmaster-single-main-content" >' . $content . '</div>'; // tourmaster-single-main-content
			echo '</div>'; // tourmaster-page-content
			echo '</div>'; // tourmaster-container
		}
	}

	if( !post_password_required() ){
		do_action('gdlr_core_print_page_builder');
	}

	////////////////////////////////////////////////////////////////////
	// review section
	////////////////////////////////////////////////////////////////////
	$review_num_fetch = 5;
	$review_args = array(
		'tour_id' => get_the_ID(), 
		'review_score' => 'IS NOT NULL',
	);
	$results = tourmaster_get_booking_data($review_args, array(
		'num-fetch' => $review_num_fetch,
		'paged' => 1,
		'orderby' => 'review_date',
		'order' => 'desc'
	), 'user_id, review_score, review_type, review_description, review_date');
	
	if( !empty($results) ){
		$max_num_page = intval(tourmaster_get_booking_data($review_args, null, 'COUNT(*)')) / $review_num_fetch;

		echo '<div class="tourmaster-single-review-container tourmaster-container" >';
		echo '<div class="tourmaster-single-review-item tourmaster-item-pdlr" >';
		echo '<div class="tourmaster-single-review" id="tourmaster-single-review" >';

		echo '<div class="tourmaster-single-review-head clearfix" >';
		echo '<div class="tourmaster-single-review-head-info clearfix" >';
		echo $tour_style->get_rating('plain');

		echo '<div class="tourmaster-single-review-filter" id="tourmaster-single-review-filter" >';
		echo '<div class="tourmaster-single-review-sort-by" >';
		echo '<span class="tourmaster-head" >' . esc_html__('Sort By:', 'tourmaster') . '</span>';
		echo '<span class="tourmaster-sort-by-field" data-sort-by="rating" >' . esc_html__('Rating', 'tourmaster') . '</span>';
		echo '<span class="tourmaster-sort-by-field tourmaster-active" data-sort-by="date" >' . esc_html__('Date', 'tourmaster') . '</span>';
		echo '</div>'; // tourmaster-single-review-sort-by
		echo '<div class="tourmaster-single-review-filter-by tourmaster-form-field tourmaster-with-border" >';
		echo '<div class="tourmaster-combobox-wrap" >';
		echo '<select id="tourmaster-filter-by" >';
		echo '<option value="" >' . esc_html__('Filter By', 'tourmaster'). '</option>';
		echo '<option value="solo" >' . esc_html__('Solo', 'tourmaster'). '</option>';
		echo '<option value="couple" >' . esc_html__('Couple', 'tourmaster'). '</option>';
		echo '<option value="family" >' . esc_html__('Family', 'tourmaster'). '</option>';
		echo '</select>';
		echo '</div>'; // tourmaster-combobox-wrap
		echo '</div>'; // tourmaster-single-review-filter-by
		echo '</div>'; // tourmaster-single-review-filter
		echo '</div>'; // tourmaster-single-review-head-info
		echo '</div>'; // tourmaster-single-review-head

		echo '<div class="tourmaster-single-review-content" id="tourmaster-single-review-content" ';
		echo 'data-tour-id="' . esc_attr(get_the_ID()) . '" ';
		echo 'data-ajax-url="' . esc_attr(TOURMASTER_AJAX_URL) . '" >';
		echo tourmaster_get_review_content_list($results);

		echo tourmaster_get_review_content_pagination($max_num_page);
		echo '</div>'; // tourmaster-single-review-content
		echo '</div>'; // tourmaster-single-review
		echo '</div>'; // tourmaster-single-review-item
		echo '</div>'; // tourmaster-single-review-container
	} 

	echo '</div>'; // tourmaster-template-wrapper

	echo '</div>'; // tourmaster-page-wrapper
get_footer(); 

?>