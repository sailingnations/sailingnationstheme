<?php

if(is_admin()) {
	include 'includes/custom-meta-boxes.php';
	include 'includes/tour-import.php';

	include 'includes/post-types.php';
}

include 'includes/avatars.php';
include 'includes/aq_resizer.php';

add_filter('stylesheet_uri', 'use_parent_theme_stylesheet');
function use_parent_theme_stylesheet()
{
    //return get_template_directory_uri() . '/style.css?'.filemtime(get_template_directory().'/style.css');
    return get_stylesheet_directory_uri() . '/style.css?'.filemtime(get_stylesheet_directory().'/style.css');
}

function sailingnations_setup() {

	if (!current_user_can('administrator') && !is_admin()) {
		show_admin_bar(false);
	}

}

add_action( 'after_setup_theme', 'sailingnations_setup' );

function sn_scripts() {
	wp_enqueue_script( 'sailingnations-script', get_stylesheet_directory_uri() . '/js/sailingnations.js?'.filemtime(get_stylesheet_directory().'/js/sailingnations.js'), array( 'jquery' ), rand(10,500), true );
}
add_action( 'wp_enqueue_scripts', 'sn_scripts' );

	function head_google_tag() { ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PBPDH3');</script>
<!-- End Google Tag Manager -->
<?	}
	add_action( 'wp_head', 'head_google_tag' );

	add_action( 'wp_footer', 'footer_google_tag');
	function footer_google_tag() {
?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PBPDH3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php
	}

	add_filter('tourmaster_custom_post_slug', 'custom_tourmaster_post_slug', 11, 1);
	function custom_tourmaster_post_slug($param) {
		if($param === 'tour-tag') {
			return 't';
		}
		if($param === 'tour-category') {
			return 'tours';
		}

		return $param;
	}

	
	add_filter('tourmaster_tour_options', 'custom_tourmaster_tour_options', 11, 1);
	function custom_tourmaster_tour_options($opt) {

		if(isset($opt['tour-settings']['options'])) {
			
			$tmp = [];

			foreach($opt['tour-settings']['options'] as $k => $v) {
				$tmp[$k] = $v;
				if($k === 'tour-price-text') {
					$tmp['fixed-discount'] = [
						'title' => 'Fixed Discount',
						'type' => 'text',
						'default' => '0',
						'description' => 'The exact amount to be discounted from the tour prices.'
					];
				}
				if($k === 'tour-price-text') {
					$tmp['tour-expired'] = [
						'title' => 'Tour Expired',
						'type' => 'combobox',
						'options' => array(
							'0' => esc_html__('No', 'tourmaster'),
							'1' => esc_html__('Yes', 'tourmaster'),
						),
						'value' => '0',
					];
				}
			}
			$opt['tour-settings']['options'] = $tmp;
		}

		$opt['date-price']['options']['date-price']['options']['adult-price']['title'] = 'Male';
		$opt['date-price']['options']['date-price']['options']['children-price']['title'] = 'Female';
		unset($opt['date-price']['options']['date-price']['options']['student-price']);
		unset($opt['date-price']['options']['date-price']['options']['infant-price']);

		$opt['date-price']['options']['date-price']['options']['additional-adult']['title'] = 'Additional Male';
		$opt['date-price']['options']['date-price']['options']['additional-children']['title'] = 'Additional Female';
		unset($opt['date-price']['options']['date-price']['options']['additional-student']);
		unset($opt['date-price']['options']['date-price']['options']['additional-infant']);

		$templates = new WP_Query(array(
			'post_type' => 'yacht_templates',
			'post_status' => 'publish',
			'numberposts' => -1,
			'posts_per_page' => -1,
		));

		$available_templates = array('0' => esc_html__('None (manual entry)', 'tourmaster'));
		if(sizeof($templates->posts) > 0) {

			foreach ($templates->posts as $post) {
				$available_templates[(string)$post->ID] = get_the_title($post->ID);
			}
		}

		$opt['date-price']['options']['date-price']['options']['template'] = array(
			'title' => esc_html__('Yacht Template', 'tourmaster'),
			'type' => 'combobox',
			'options' => $available_templates,
			'condition' => array('pricing-room-base' => 'enable'),
		);

		$opt['date-price']['options']['date-price']['options']['num-rooms'] = array(
			'title' => esc_html__('Number of cabins', 'tourmaster'),
			'type' => 'combobox',
			'options' => array(
				'1' => esc_html__('1 cabin', 'tourmaster'),
				'2' => esc_html__('2 cabins', 'tourmaster'),
				'3' => esc_html__('3 cabins', 'tourmaster'),
				'4' => esc_html__('4 cabins', 'tourmaster'),
				'5' => esc_html__('5 cabins', 'tourmaster'),
			),
			'value' => '5',
			'condition' => array('pricing-room-base' => 'enable', 'template' => '0'),
		);

		$opt['date-price']['options']['date-price']['options']['per-cabin-pricing'] = array(
			'title' => esc_html__('Per-Cabin Pricing', 'tourmaster'),
			'type' => 'combobox',
			'options' => array(
				'0' => esc_html__('No', 'tourmaster'),
				'1' => esc_html__('Yes', 'tourmaster'),
			),
			'value' => '0',
			'condition' => array('pricing-room-base' => 'enable', 'template' => '0'),
		);

		for($i = 1; $i <= 5; $i++) {
			$range = array();
			for($j = $i; $j <= 5; $j++)
				$range[] = (string)$j;

			$opt['date-price']['options']['date-price']['options']["room$i-name"] = 
			array(
				'title' => esc_html__("Cabin $i Name", 'tourmaster'),
				'type' => 'text',
				'condition' => array('pricing-room-base' => 'enable'/*, 'pricing-method' => 'variable'*/, 'num-rooms' => $range, 'template' => '0'),
			);
			$opt['date-price']['options']['date-price']['options']["room$i-price"] = 
			array(
				'title' => esc_html__("Cabin $i Price", 'tourmaster'),
				'type' => 'text',
				'condition' => array('pricing-room-base' => 'enable'/*, 'pricing-method' => 'variable'*/, 'num-rooms' => $range, 'template' => '0')
			);
			$opt['date-price']['options']['date-price']['options']["room$i-spots"] = 
			array(
				'title' => esc_html__("Cabin $i Spots", 'tourmaster'),
				'type' => 'text',
				'condition' => array('pricing-room-base' => 'enable'/*, 'pricing-method' => 'variable'*/, 'num-rooms' => $range, 'template' => '0')
			);
			$opt['date-price']['options']['date-price']['options']["room$i-description"] = 
			array(
				'title' => esc_html__("Cabin $i Description", 'tourmaster'),
				'type' => 'textarea',
				'condition' => array('pricing-room-base' => 'enable'/*, 'pricing-method' => 'variable'*/, 'num-rooms' => $range, 'template' => '0')
			);
		}
										
		return $opt;
	}

	function debugging() { // debugging enabled for IP
		return $_SERVER['REMOTE_ADDR'] === '31.185.123.53';
	}

	function getOccupiedCabins($booking_detail, $date_price) { // custom
		$a = array();
		if(!isset($booking_detail['tour-male']))
			return $a;
		
		$template = $date_price['template'];
 
		if($template) {
			$cabins = get_post_meta($template, '_yacht_cabins', true);
			$per_cabin_pricing = (bool)get_post_meta($template, '_yacht_per_cabin_pricing', true);
			$date_price['num-rooms'] = count($cabins);
		}
		else {
			$per_cabin_pricing = (bool)$date_price['per-cabin-pricing'];
		}

		foreach($booking_detail['tour-male'] as $cabin_index => $num_passengers) {

			if($template) {
				$cabin_spots = $cabins[$cabin_index]['spots'];
				$cabin_name = $cabins[$cabin_index]['name'];
				$cabin_price = $cabins[$cabin_index]['price'];
				$cabin_descr = $cabins[$cabin_index]['descr'];
			}
			elseif(isset($date_price['room'.($cabin_index+1).'-spots'])) {
				$cabin_spots = $date_price['room'.($cabin_index+1).'-spots'];
				$cabin_name = $date_price['room'.($cabin_index+1).'-name'];
				$cabin_price = $date_price['room'.($cabin_index+1).'-price'];
				$cabin_descr = $date_price['room'.($cabin_index+1).'-descr'];
			}


			$male_passengers = $num_passengers;
			$female_passengers = $booking_detail['tour-female'][$cabin_index];
			if(!empty($male_passengers) || !empty($female_passengers)) {
				$a[] = array(
					'cabin_index' => $cabin_index,
					'cabin_name' => $cabin_name,
					'cabin_price' => $cabin_price,
					'cabin_descr' => $cabin_descr,
					'male' => (int)$male_passengers,
					'female' => (int)$female_passengers,
					'per_cabin_pricing' => $per_cabin_pricing,
					'cabin_spots' => $cabin_spots
				);
			}
		}
		return $a;
	}

	add_filter('tourmaster_user_content_template', 'custom_tourmaster_user_content_template', 10, 2);
	function custom_tourmaster_user_content_template($template, $page_type) {
		if($page_type === 'edit-profile') {
			return get_stylesheet_directory()."/tourmaster/single/user/$page_type.php";
		}			
		return $template;
	}
	
	
	
	
	
	
	
function get_image_sizes( $size = '' ) {

    global $_wp_additional_image_sizes;

    $sizes = array();
    $get_intermediate_image_sizes = get_intermediate_image_sizes();

    // Create the full array with sizes and crop info
    foreach( $get_intermediate_image_sizes as $_size ) {

        if ( in_array( $_size, array( 'thumbnail', 'medium', 'large' ) ) ) {

            $sizes[ $_size ]['width'] = get_option( $_size . '_size_w' );
            $sizes[ $_size ]['height'] = get_option( $_size . '_size_h' );
            $sizes[ $_size ]['crop'] = (bool) get_option( $_size . '_crop' );

        } elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {

            $sizes[ $_size ] = array(
                'width' => $_wp_additional_image_sizes[ $_size ]['width'],
                'height' => $_wp_additional_image_sizes[ $_size ]['height'],
                'crop' =>  $_wp_additional_image_sizes[ $_size ]['crop']
            );

        }

    }

    // Get only 1 size if found
    if ( $size ) {

        if( isset( $sizes[ $size ] ) ) {
            return $sizes[ $size ];
        } else {
            return false;
        }

    }

    return $sizes;
}

add_action('wp_ajax_upload_user_profile_image','upload_user_profile_image_call');
add_action('wp_ajax_nopriv_upload_user_profile_image','upload_user_profile_image_call');
function upload_user_profile_image_call() {
	ob_start('ob_gzhandler');
    $messages = array();
    $result = array();

    if ( 0 < $_FILES['file']['error'] ) {
        $messages[] = 'Error: ' . $_FILES['file']['error'];
        $result['status'] = false;
    }
    else {
        $available_types = array('image/jpeg','image/png');

        if(in_array($_FILES['file']['type'],$available_types)) {
            $current_user = wp_get_current_user();
            $user_id = $current_user->ID;

            $upload_dir = wp_upload_dir();

            $user_avatar_path = $upload_dir['basedir'] . '/avatars/' . $user_id;
            $user_avatar_path_with_url = $upload_dir['baseurl'] . '/avatars/' . $user_id;

            if (!file_exists($user_avatar_path)) {
                mkdir($user_avatar_path, 0777, true);
            }
			else {
				foreach(glob($user_avatar_path.'/*', GLOB_NOSORT) as $file)
					unlink($file);
			}

			$file_name = time() . '.' . pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $file = $user_avatar_path . '/' . $file_name;
            $file_with_url = $user_avatar_path_with_url . '/' . $file_name;

            move_uploaded_file($_FILES['file']['tmp_name'], $file);
            update_user_meta( $user_id, 'user_meta_image', $file_with_url );

            $thumb_sizes = get_image_sizes('medium');
            $avatar_img = aq_resize($file_with_url,$thumb_sizes['width'],$thumb_sizes['height'],true,true,true);

            $result['image'] = array(
                'url' => $avatar_img
            );
            $result['status'] = true;
        } else {
            $messages[] = _x('Error: Your file type is not supported','sailingnations');
            $result['status'] = false;
        }
    }

    $result['messages'] = $messages;

    die(json_encode($result));
}



function modify_query_order_tag( $query ) {
	
   if ( $query->is_archive() && ($query->is_tax('tour_category') || $query->is_tax('tour_tag')) && $query->is_main_query() && !is_admin() ) {
	  $query->set( 'orderby', 'meta_value_num' );
	  $query->set( 'meta_key', 'tour-next-date' );
	  $query->set( 'order', 'ASC' );
	  $meta_query = $query->get('meta_query');
	  $meta_query[] = 
		array(
			'key' => 'tourmaster-tour-option',
			'compare' => 'NOT LIKE',
			'value' => '"tour-expired";s:1:"1"'
		);
	
		$query->set('meta_query',$meta_query);
   }

}
add_action( 'pre_get_posts', 'modify_query_order_tag' );


add_action('init', 'custom_rewrite_basic');
function custom_rewrite_basic() {
    add_rewrite_tag('%location%','([^&]+)');
    add_rewrite_tag('%duration%','([^&]+)');
    add_rewrite_tag('%date%','([^&]+)');
    add_rewrite_tag('%sort%','([^&]+)');
    add_rewrite_tag('%tour-search%','([^&]+)');
    add_rewrite_tag('%seo%','([^&]+)');

    add_rewrite_rule(
        '^sailing/(.*?)/(.*?)/(.*?)/(.*?)/?$',
        'index.php?pagename=search-tours-sailing-holidays&tour-search=1&location=$matches[1]&duration=$matches[3]&date=$matches[2]&sort=$matches[4]',
        'top'
    );
	//flush_rewrite_rules();
	
}
