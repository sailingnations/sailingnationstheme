if (!String.prototype.endsWith) {
  String.prototype.endsWith = function(searchString, position) {
      var subjectString = this.toString();
      if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
        position = subjectString.length;
      }
      position -= searchString.length;
      var lastIndex = subjectString.indexOf(searchString, position);
      return lastIndex !== -1 && lastIndex === position;
  };
}

(function($){
	$('.tourmaster-tour-view-more-dates').click(function(event) {
		var divs = $(this).siblings('div.hidden');
		if($(this).html() === 'More Dates') {
			$(this).html('Less Dates');
			divs.slideDown("fast");
		}
		else {
			$(this).html('More Dates');
			divs.slideUp("fast");
		}
		event.preventDefault();
	});
	
	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	var form = $('#tourmaster-single-tour-booking-fields');
	if(form.length) {
		var ajax_url = form.attr('data-ajax-url');
		
		form.on('change', '[data-step="3"] select[name]', function(){
			
			var num_people = 0;
			var tour_id = $('input[name="tour-id"]').val();
			var tour_date = $('input[name="tour-date"]').val();
			var passengers = {};
			var tour_male = {};
			var tour_female = {};

			form.find('[data-step="3"]').each(function(){

				var cabin = parseInt($(this).data('cabin-id'))-1;
				$(this).find('select[name]').each(function(){
					
					if(!(cabin in passengers)) {
						passengers[cabin] = {male: 0, female: 0};
						tour_male[cabin] = 0;
						tour_female[cabin] = 0;
					}

					if( $(this).val() != "" ){
						var count = parseInt($(this).val());
						num_people += count;
						if($(this).attr('name') === 'tour-male[]') {
							passengers[cabin]['male'] += count;
							tour_male[cabin] += count;
						}
						else if($(this).attr('name') === 'tour-female[]') {
							passengers[cabin]['female'] += count;
							tour_female[cabin] += count;
						}
					}
				});
			});

			
			var data = {
				action: "get_tour_prices",
				data: {
					"tour-id": tour_id,
					"tour-date": tour_date,
					"tour-male": tour_male,
					"tour-female": tour_female
				}
			};
			//console.log(data);
			
			$.post(ajax_url, data,
				function( data ){
					if( typeof(data['total-price']) != 'undefined' ){
						var totalPrice = parseInt(data['total-price'], 10);
						if(totalPrice < 1)
							return;

						totalPrice = numberWithCommas(totalPrice);

						var discount = $('.tourmaster-tour-discount-price');
						if(discount.length) {
							$('.tourmaster-tour-price').hide();
							discount.html("€" + totalPrice);
						}
						else {							
							var pricing = $('.tourmaster-tour-price');
							pricing.find('.tourmaster-head').hide();
							pricing.find('.tourmaster-tail').html("€" + totalPrice);
						}
					}
				}
			);

		});
	}

	

	/*$('body').on('click', '[data-action="update-extras"]', function() {

		var data = {
			action: 'update_extras',
			extras: $('[data-action="update-extras"]:checked').map(function() { return this.value; }).get().join(),
		}

		$.ajax({
			method: "post",
			url: "/wp-admin/admin-ajax.php",
			beforeSend: function() {
				$('a.tourmaster-tour-booking-continue').removeClass('tourmaster-payment-step').animate({backgroundColor:'#e1e1e1'}, 300);
			},
			data: data,
			success: function(res) {
				setTimeout(function(){
					$('a.tourmaster-tour-booking-continue').addClass('tourmaster-payment-step').animate({backgroundColor:'#4674e7'}, 300);
				}, 500);

				if(res.status) {
					Theme.getBookingContent();
				} else {
					alert(res.messages)
				}

			}
		});

		return true;
	});*/

})(jQuery);