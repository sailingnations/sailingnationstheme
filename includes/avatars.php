<?php
function user_avatar_extra_profile_fields( $user ) {
    wp_enqueue_media();

    $attachment_id = get_the_author_meta( 'user_meta_image_id', $user->ID );

    $avatar_img = '';

    if(empty($attachment_id)) {

        $thumb_sizes = get_image_sizes('thumbnail');
        $user_fb_image =  get_the_author_meta( 'user_meta_image', $user->ID );
        $avatar_img = aq_resize($user_fb_image,$thumb_sizes['width'],$thumb_sizes['height'],true,true,true);
    } else {
        $image_thumb = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
        $avatar_img = $image_thumb[0];
    } ?>

    <script>
        jQuery(document).ready(function($){
            // Uploading files
            var file_frame;

            $('.additional-user-image').on('click', function( event ){

                event.preventDefault();

                // If the media frame already exists, reopen it.
                if ( file_frame ) {
                    file_frame.open();
                    return;
                }

                // Create the media frame.
                file_frame = wp.media.frames.file_frame = wp.media({
                    title: $( this ).data( 'uploader_title' ),
                    button: {
                        text: $( this ).data( 'uploader_button_text' ),
                    },
                    multiple: false  // Set to true to allow multiple files to be selected
                });

                // When an image is selected, run a callback.
                file_frame.on( 'select', function() {
                    // We set multiple to false so only get one image from the uploader
                    attachment = file_frame.state().get('selection').first().toJSON();

                    console.log(attachment);

                    $('#user-avatar-img').attr('src',attachment.sizes.thumbnail.url);
                    $('#user_meta_image').val(attachment.url);
                    $('#user_meta_image_id').val(attachment.id);

                    // Do something with attachment.id and/or attachment.url here
                });

                // Finally, open the modal
                file_frame.open();
            });

        });
    </script>

    <h3><?php _e( 'Avatar', 'textdomain' ); ?></h3>

    <table class="form-table">

        <tr>
            <th><label for="user_meta_image"><?php _e( 'Select Image', 'textdomain' ); ?></label></th>
            <td>
                <!-- Outputs the image after save -->
                <img id="user-avatar-img" src="<?php echo $avatar_img; ?>" style="width:150px;"><br />
                <!-- Outputs the text field and displays the URL of the image retrieved by the media uploader -->
                <input type="hidden" name="user_meta_image" id="user_meta_image" value="<?php echo esc_url_raw( get_the_author_meta( 'user_meta_image', $user->ID ) ); ?>" class="regular-text" />
                <input type="hidden" name="user_meta_image_id" id="user_meta_image_id" value="<?php echo get_the_author_meta( 'user_meta_image_id', $user->ID ); ?>" class="regular-text" />
                <!-- Outputs the save button -->
                <input type='button' class="additional-user-image button-primary" value="<?php _e( 'Upload Image', 'textdomain' ); ?>" id="uploadimage"/><br />
                <!--<span class="description"><?php /*_e( 'Upload an additional image for your user profile.', 'textdomain' ); */?></span>-->
            </td>
        </tr>

    </table><!-- end form-table -->
<?php } // additional_user_fields

add_action( 'show_user_profile', 'user_avatar_extra_profile_fields' );
add_action( 'edit_user_profile', 'user_avatar_extra_profile_fields' );

function save_user_avatar_extra_profile_fields( $user_id ) {

    // only saves if the current user can edit user profiles
    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;

    update_user_meta( $user_id, 'user_meta_image', $_POST['user_meta_image'] );
    update_user_meta( $user_id, 'user_meta_image_id', $_POST['user_meta_image_id'] );
}

add_action( 'personal_options_update', 'save_user_avatar_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'save_user_avatar_extra_profile_fields' );

add_filter( 'get_avatar' , 'display_custom_user_avatar' , 1 , 5 );

function display_custom_user_avatar(  $avatar, $id_or_email, $size, $default, $alt ) {
    $user = false;

    if ( is_numeric( $id_or_email ) ) {

        $id = (int) $id_or_email;
        $user = get_user_by( 'id' , $id );

    } elseif ( is_object( $id_or_email ) ) {

        if ( ! empty( $id_or_email->user_id ) ) {
            $id = (int) $id_or_email->user_id;
            $user = get_user_by( 'id' , $id );
        }

    } else {
        $user = get_user_by( 'email', $id_or_email );
    }

    if ( $user && is_object( $user ) ) {

        $user_id = $user->ID;

        $attachment_id = get_the_author_meta( 'user_meta_image_id', $user_id );

        if(!empty($attachment_id) && is_numeric($attachment_id) && ($image_thumb = wp_get_attachment_image_src( $attachment_id, 'thumbnail' ))) {
            $avatar = aq_resize($image_thumb[0],$size,$size,true,true,true);

            $avatar = "<img alt='{$alt}' src='{$avatar}' class='avatar avatar-{$size} photo' height='{$size}' width='{$size}' />";
        } else {
            $user_fb_image = get_the_author_meta( 'user_meta_image', $user_id );
            $avatar_img = '';

            if(empty($user_fb_image)) {
				$avatar = '';

				$gender = get_user_meta( $user_id, 'customer_gender', true);
				if($gender === '') {
					$gender = get_user_meta( $user_id, 'gender', true);
					if($gender !== '')
						$avatar = $gender;
					else
						$avatar = 'female';
				}
				else {
					if($gender == 1)
						$avatar = 'male';
					else
						$avatar = 'female';
				}

				$avatar_img = get_stylesheet_directory_uri().'/images/avatar_'.$avatar.'.png';
                //$avatar_img = "https://placeholdit.imgix.net/~text?txtsize=14&txt=NO%20PHOTO&w={$size}&h={$size}";
            } else {
				//$user_fb_image = str_replace('https://www.sailingnations.com', 'http://snpress.sailingnations.com', $user_fb_image);
                $avatar_img = aq_resize($user_fb_image,$size,$size,true,true,true);
            }

            $avatar = "<img alt='{$alt}' src='{$avatar_img}' class='avatar avatar-{$size} photo' height='{$size}' width='{$size}' />";
        }

    }

    return $avatar;
}
