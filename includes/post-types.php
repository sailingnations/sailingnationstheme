<?php
function yacht_post_type_init() {
    $args = array(
        'labels' => array(
            'name' => __( 'Yachts' ),
            'singular_name' => __( 'yacht' )
        ),
        'public'             => false,
        'publicly_queryable' => false,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'yacht' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor' )
    );

    register_post_type( 'yacht', $args );

}

add_action( 'init', 'yacht_post_type_init' );

function yacht_templates_post_type_init() {
    $args = array(
        'labels' => array(
            'name' => __( 'Yacht Templates' ),
            'singular_name' => __( 'yacht_template' )
        ),
        'public'             => false,
        'publicly_queryable' => false,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'yacht_template' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title' )
    );

    register_post_type( 'yacht_templates', $args );
}

add_action( 'init', 'yacht_templates_post_type_init' );


function departure_locations_post_type_init() {
    $args = array(
        'labels' => array(
            'name' => __( 'Departure Locations' ),
            'singular_name' => __( 'departure_locations' )
        ),
        'public'             => false,
        'publicly_queryable' => false,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'departure_location' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title')
    );

    register_post_type( 'departure_locations', $args );
}

add_action( 'init', 'departure_locations_post_type_init' );

function extra_options_post_type_init() {
    $args = array(
        'labels' => array(
            'name' => __( 'Extra Options' ),
            'singular_name' => __( 'extra_option' )
        ),
        'public'             => false,
        'publicly_queryable' => false,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'extra-options' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor')
    );

    register_post_type( 'extra_options', $args );
}

add_action( 'init', 'extra_options_post_type_init' );
