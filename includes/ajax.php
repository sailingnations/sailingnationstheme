<?php

add_action('wp_ajax_get_tour_prices','ajax_get_tour_prices');
add_action('wp_ajax_nopriv_get_tour_prices','ajax_get_tour_prices');
function ajax_get_tour_prices() {
	ob_start('ob_gzhandler');

    //echo '<pre>';print_r($_POST);echo '</pre>';die;

    $result = array();

	ini_set('display_errors', 1);
	error_reporting(E_ALL);


	$price_breakdown['room'] = array();


	$types = array('male', 'female');

	$booking_detail = $_POST['data'];
	if(!$booking_detail) {
		wp_send_json(['total-price' => 0]);
	}

	$tour_option = tourmaster_get_post_meta($booking_detail['tour-id'], 'tourmaster-tour-option');
	$date_price = tourmaster_get_tour_date_price($tour_option, $booking_detail['tour-id'], $booking_detail['tour-date']);

	$total_price = 0;
	$passenger_count = 0;
	$booked_cabins = getOccupiedCabins($booking_detail, $date_price);

	foreach($booked_cabins as $i => $cabin) {
		$room = array();
		$room['price'] = $cabin['cabin_price'];
		if(isset($tour_option['fixed-discount'])) {
			$room['price'] -= $tour_option['fixed-discount'];
		}
		$room['cabin_name'] = $cabin['cabin_name'];
		$room['cabin_spots'] = $cabin['cabin_spots'];
		$room['per_cabin_pricing'] = $cabin['per_cabin_pricing'];

		foreach( $types as $type ) {

			$room[$type . '-amount'] = $cabin[$type];
			$passenger_count += $room[$type . '-amount'];

			if(!$room['per_cabin_pricing'])
				$total_price += $room[$type . '-amount'] * $room['price'];

		}

		if($room['per_cabin_pricing']) {
			$total_price += $room['price'];
		}

		$price_breakdown['room'][] = $room;
	}

	// calculate total base price
	foreach( $types as $type ){
		if( !empty($price_breakdown[$type . '-amount']) ){
			$price_breakdown[$type . '-base-price'] = $date_price[$type . '-price'];
			$total_price += $price_breakdown[$type . '-base-price'] * $price_breakdown[$type . '-amount'];
		}	
	}

	$price_breakdown['sub-total-price'] = $total_price;

	if( !empty($booking_detail['coupon-code']) ){
		$coupon_validate = tourmaster_validate_coupon_code($booking_detail['coupon-code'], $booking_detail['tour-id']);
		if( !empty($coupon_validate['data']) ){
			$coupon_data = $coupon_validate['data'];

			$price_breakdown['coupon-code'] = $booking_detail['coupon-code'];
			if( $coupon_data['coupon-discount-type'] == 'percent' ){
				$price_breakdown['coupon-text'] = $coupon_data['coupon-discount-amount'] . '%';
				$price_breakdown['coupon-amount'] = (floatval($coupon_data['coupon-discount-amount']) * $total_price) / 100;
			}else if( $coupon_data['coupon-discount-type'] == 'amount' ){
				$price_breakdown['coupon-amount'] = $coupon_data['coupon-discount-amount'];
			}

			if( $price_breakdown['coupon-amount'] > $total_price ){
				$total_price = 0;
			}else{
				$total_price = $total_price - $price_breakdown['coupon-amount'];
			}
		}
	}
	
	if( !empty($booking_detail['options']) ){
		$options = explode(",", $booking_detail['options']);

		foreach($options as $ID) {
			
			$title = get_the_title($ID);
			$price = get_post_meta($ID, '_option_price_eur', true);
			
			$total_price += $price * $passenger_count;
			$price_breakdown['sub-total-price'] += $price * $passenger_count;
			

			$price_breakdown['options'][] = [$title, $price];
		
		}
	}
	
	
	

	$tax_rate = tourmaster_get_option('general', 'tax-rate', 0);
	if( !empty($tax_rate) ){
		$price_breakdown['tax-rate'] = $tax_rate;
		$price_breakdown['sub-total-price'] /= 1 + $price_breakdown['tax-rate'] / 100;
		$price_breakdown['tax-due'] = $total_price - $price_breakdown['sub-total-price'];
	}

	$ret = array(
		'total-price' => $total_price,
		'price-breakdown' => $price_breakdown
	);

    wp_send_json($ret);

}


