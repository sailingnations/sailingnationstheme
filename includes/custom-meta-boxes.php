<?php


function yacht_templates_meta_box() {

    $screens = array( 'yacht_templates' );

    foreach ( $screens as $screen ) {

        add_meta_box(
            'yacht_tpl',
            __( 'Template Settngs', 'sailingnations_admin' ),
            'yacht_templates_meta_box_callback',
            $screen,
            //'side',
            'normal',
            'high'
        );
    }
}
add_action( 'add_meta_boxes', 'yacht_templates_meta_box' );

function yacht_templates_meta_box_callback( $post ) {

    wp_nonce_field( 'yacht_templates_save_meta_box_data', 'yacht_templates_meta_box_nonce' );

    /*
     * Use get_post_meta() to retrieve an existing value
     * from the database and use the value for the form.
     */
    $yacht_cabins = get_post_meta( $post->ID, '_yacht_cabins', true );
    $yacht_per_cabin_pricing = get_post_meta( $post->ID, '_yacht_per_cabin_pricing', true );

    //echo '<pre>';print_r($event_dates);echo '</pre>';

    //echo '<pre>';print_r($event_dates);echo '</pre>'; ?>

    <style>
        .cabins-container .cabins-box{margin-bottom: 10px;border:1px solid #eee;padding: 10px;}
        .cabins-container .cabins-box .remove-event-box{margin-top: 3px;}
        #cloned-date-box{display: none;}
        #cloned-date-box .remove-event-box{display: none;}
    </style>

    <script>
        jQuery(window).on('load', function() {
            jQuery('[data-action="yacht-add-new-cabin-box"]').on('click', function () {
                jQuery('.cabins-container').append('<div class="cabins-box cloned">Name: <input placeholder="Cabin name" required type="text" value="" name="yacht_cabin_name[]">&nbsp;Price: <input required placeholder="In EUR" type="number" name="yacht_cabin_price[]" value="">&nbsp;Spots: <input required type="number" name="yacht_cabin_spots[]" min="1" max="2" value="2">&nbsp;<a class=" button button-small remove-event-box" href="#">Remove</a><div style="width: 100%; margin-top: 10px;"><span style="vertical-align: top;">Description:</span>&nbsp;<textarea name="yacht_cabin_descr[]" rows="3" cols="70" style="resize: vertical;"></textarea></div></div>');
            });

            jQuery('body').on('click', '.cabins-box.cloned .remove-event-box', function() {
                if (confirm("Are you sure?")) {
                    var box = jQuery(this).closest('.cloned');

                    box.slideUp("fast", function() {
                        jQuery(this).remove();
                    });
                }

                return false;
            });
        });
    </script>
    <div class="cabins-container">
        <label><input type="checkbox" name="per_cabin_pricing" value="1" <? if($yacht_per_cabin_pricing) echo 'checked'; ?>> Per-cabin pricing</label>
        <?php if(!empty($yacht_cabins)) { ?>
        
            <?php foreach ($yacht_cabins as $cabin) { ?>

                <div class="cabins-box cloned">Name: <input placeholder="Cabin name" required type="text" value="<?php echo $cabin['name'] ?>" name="yacht_cabin_name[]">&nbsp;Price: <input required placeholder="In EUR" type="number" name="yacht_cabin_price[]" value="<?php echo $cabin['price']; ?>">&nbsp;Spots: <input required type="number" name="yacht_cabin_spots[]" min="1" max="2" value="<?php echo $cabin['spots']; ?>">&nbsp;<a class=" button button-small remove-event-box" href="#">Remove</a><div style="width: 100%; margin-top: 10px;"><span style="vertical-align: top;">Description:</span>&nbsp;<textarea name="yacht_cabin_descr[]" rows="3" cols="70" style="resize: vertical;"><?php echo $cabin['descr']; ?></textarea></div></div>

            <?php } ?>
            
        <?php } ?>
        
    </div>
    <a href="javascript:;" data-action="yacht-add-new-cabin-box" class="button button-primary button-small">Add Cabin</a>
<?php }

function yacht_templates_save_meta_box_data( $post_id ) {

    /*
     * We need to verify this came from our screen and with proper authorization,
     * because the save_post action can be triggered at other times.
     */

    // Check if our nonce is set.
    if ( ! isset( $_POST['yacht_templates_meta_box_nonce'] ) ) {
        return;
    }

    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['yacht_templates_meta_box_nonce'], 'yacht_templates_save_meta_box_data' ) ) {
        return;
    }

    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    // Check the user's permissions.
    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return;
        }

    } else {

        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
    }

    if ( ! isset( $_POST['yacht_cabin_name'] ) || ! isset( $_POST['yacht_cabin_price'] ) || ! isset( $_POST['yacht_cabin_spots'] ) || ! isset( $_POST['yacht_cabin_descr'] )) {
        return;
    }

    $cabins = array();

    if(sizeof($_POST['yacht_cabin_name']) > 0 && sizeof($_POST['yacht_cabin_price']) > 0 && sizeof($_POST['yacht_cabin_spots']) > 0 && sizeof($_POST['yacht_cabin_descr']) > 0) {
        foreach ($_POST['yacht_cabin_name'] as $key => $name) {
            $cabins[$key] = array(
                'name' => $name,
                'price' => $_POST['yacht_cabin_price'][$key],
                'spots' => $_POST['yacht_cabin_spots'][$key],
                'descr' => $_POST['yacht_cabin_descr'][$key]
            );
        }

        update_post_meta( $post_id, '_yacht_cabins', $cabins );
        update_post_meta( $post_id, '_yacht_per_cabin_pricing', isset($_POST['per_cabin_pricing']) );
    }

}
add_action( 'save_post', 'yacht_templates_save_meta_box_data' );




add_filter( 'rwmb_meta_boxes', 'extra_options_meta_boxes' );

function extra_options_meta_boxes( $meta_boxes ) {

    $prefix = '_option_';

    $post_types = array('extra_options');

    $meta_boxes[] = array(
        // Meta box id, UNIQUE per meta box. Optional since 4.1.5
        'id'         => 'extra_options_answers',

        // Meta box title - Will appear at the drag and drop handle bar. Required.
        'title'      => __( 'Properties', 'sailingnations' ),

        // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
        'post_types' => $post_types,

        // Where the meta box appear: normal (default), advanced, side. Optional.
        'context'    => 'normal',

        // Order of meta box: high (default), low. Optional.
        'priority'   => 'high',

        // Auto save: true, false (default). Optional.
        'autosave'   => true,

        // List of meta fields
        'fields'     => array(
            // TEXT
            array(
                'name'  => __( 'Short description', 'sailingnations' ),
                'id'    => "{$prefix}short_desc",
                'type'  => 'textarea'
            ),
            array(
                'name'  => __( 'Price (EUR)', 'sailingnations' ),
                'id'    => "{$prefix}price_eur",
                'type'  => 'number',
				'required' => true,
            ),
        )
    );

    return $meta_boxes;
}
