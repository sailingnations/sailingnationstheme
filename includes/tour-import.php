<?

	/*WordPress Menus API.*/
	function add_new_menu_items()
	{
		//add a new menu item. This is a top level menu item i.e., this menu item can have sub menus
		add_menu_page(
			"Tour Import", //Required. Text in browser title bar when the page associated with this menu item is displayed.
			"Tour Import", //Required. Text to be displayed in the menu.
			"manage_options", //Required. The required capability of users to access this menu item.
			"tourimport-options", //Required. A unique identifier to identify this menu item.
			"theme_options_page", //Optional. This callback outputs the content of the page associated with this menu item.
			"", //Optional. The URL to the menu item icon.
			100 //Optional. Position of the menu item in the menu.
		);

	}

	function theme_options_page()
	{
		?>
			<div class="wrap">
			<div id="icon-options-general" class="icon32"></div>
			<h1>Tour Import</h1>
			<form method="post" action="options.php">
				<?php
				
					//add_settings_section callback is displayed here. For every new section we need to call settings_fields.
					settings_fields("category_pairing_section");
					
					// all the add_settings_field callbacks is displayed here
					do_settings_sections("tourimport-options");
				
					// Add the submit button to serialize the options
					submit_button();
					
				?>
			</form>
		</div>
		<?php
	}

	//this action callback is triggered when wordpress is ready to add new items to menu.
	add_action("admin_menu", "add_new_menu_items");

	function display_options()
	{
		//section name, display name, callback to print description of section, page to which section is attached.
		add_settings_section("category_pairing_section", "", "tour_import_content", "tourimport-options");

		//setting name, display name, callback to print form element, page in which field is displayed, section to which it belongs.
		//last field section is optional.
		add_settings_field("category_pairing", "Tour: ", "tour_import_form", "tourimport-options", "category_pairing_section");

		//section name, form element name, callback for sanitization
		register_setting("category_pairing_section", "category_pairing");
	}

	function tour_import_content(){echo "";}
	function tour_import_form()
	{
		$tours = new WP_Query( array(
			'post_type' => 'tour',
			'num_posts' => -1,
			'posts_per_page' => -1,
			//'post_status' => array('draft','pending','publish'),
		) );

		//id and name of form element should be same as the setting name.
		?>
		<style>
		.form-table th 
		{
			width: 1px;
		}
		</style>
		<input type="hidden" name="action" value="process_import_tour">
		<table border="0" style="width: 100%;">
		<tr>
		<td style="width: 50%; padding-bottom: 0">
			<!--h2>Vendor registration</h2-->
			<select id="tour_selection" name="tour_selection" size="25" style="height: 500px; width: 100%" required>
<?	foreach($tours->posts as $k => $tour) { ?>
		<option value="<?=$tour->ID?>"><?=$tour->post_title?></option>
<? } ?>
			</select>
		</td>
		<td style="width: 50%; padding-bottom: 0">
			<!--h2>Create RFT</h2-->
			
			<!--select multiple id="create_rft" style="height: 500px; width: 100%"-->
			<textarea id="import_text" name="import_text" style="height: 500px; width: 100%" required></textarea>

		</td>
		</tr>
		</table>

		<?php
	}

//this action is executed after loads its core, after registering all actions, finds out what page to execute and before producing the actual output(before calling any action callback)
add_action("admin_init", "display_options");

add_action( 'admin_action_process_import_tour', 'process_import_tour_admin_action' );
function process_import_tour_admin_action()
{
	if(!isset($_POST['import_text']) || !isset($_POST['tour_selection']))
		return;

	ob_start();

	$tour = (int)$_POST['tour_selection'];
	if(!$tour) {
		echo 'Tour is empty. Please go <a onclick="window.history.back(); return false;" href="#">back</a> and correct this.';
		exit;
	}
	$import = (string)$_POST['import_text'];
	$update = isset($_POST['addDates']);

	$post = get_post($tour);
	$slug = $post ? $post->post_name : '';

	$cabin_names = array(); ?>
<html>
<head>
<title><?=$slug?></title>
<style>
table td {
	text-align: center;
	padding: 2px 3px;
}
button.big {
	padding: 10px; font-size: 16pt; margin: 10px; width: 200px;
}
</style>
</head>
<body>
<form method="post" action="/wp-admin/options.php">
<input type="hidden" name="action" value="process_import_tour">
<input type="hidden" name="option_page" value="category_pairing_section">
<input type="hidden" name="tour_selection" value="<?=$tour?>">
<input type="hidden" name="import_text" value="<?=str_replace('\"', '&quot;', $import)?>">


<button type="button" class="big" onclick="window.location='/wp-admin/admin.php?page=tourimport-options';">Back to Importer</button>
<button type="button" class="big" onclick="window.location='/wp-admin/post.php?post=<?=$tour?>&action=edit&lang=en';">Edit Tour</button>
<? if($slug) { ?>
<button type="button" class="big" onclick="window.location='/<?=$post->post_name?>';">View Tour</button>
<? } ?>

Input: <button onclick="if(this.innerHTML==='Show') { this.innerHTML = 'Hide'; document.getElementById('tbl_input').style.display='table';} else { document.getElementById('tbl_input').style.display='none'; this.innerHTML = 'Show'} return false">Show</button>
<table border="1" style="display: none;" id="tbl_input">
<?

	$settings = array();

	foreach(explode("\n", $import) as $line) {

		$contents = array_filter(explode("\t", $line),
			function($value) { return !empty(trim($value)); }
		);
		
		if(count($contents) < 3)
			continue;

		$num_strings = 0;
		$isHeader = false;
		foreach($contents as $i => $field) {
			$field = trim($field);
			$contents[$i] = $field;

			if(!is_numeric($field) && !strtotime($field)) {
				$num_strings++;
			}
		}

		if($num_strings > 2) {
			$isHeader = true;
			$cabins = array();
		}

		$prices = array();
		$dates = array();

		ob_start();
?>
	<tr style="background: |BGCOLOR|;">
	<!--td><?=count($contents)?></td>
	<td><?=$num_strings?></td-->
<?
		foreach($contents as $i => $field) {
			if($isHeader) {
				if($i >= 2) {
					$num_spots = 2;
					if(strpos($field, 'Triple') !== FALSE)
						$num_spots = 3;
					elseif(strpos($field, 'Single') !== FALSE)
						$num_spots = 1;

					$cabins[] = [$field, $num_spots];
				} ?>
		<th><?=$field?></th>
<?			}
			elseif(strtotime($field)) {
				$dates[] = date('Y-m-d', strtotime($field)); ?>
		<td style="color: yellow; font-weight: bold"><?=date('Y-m-d', strtotime($field))?></td>
<?			}
			else {
				$tmp = str_replace(',', '.', str_replace('.', '', $field));
				if((string)(float)$tmp === $tmp) {
					$prices[] = $tmp; ?>
		<td style="color: white;"><?=$tmp?></td>
		<?		} else { ?>
		<td style="background: red; color: black;"><?=$field?></td>
<?				}

			} 
		}

		$row = ob_get_clean();
		if(!$isHeader) {
			$hash = sha1(serialize($prices));
			$allDates[$hash][] = $dates;
			$allCabins[$hash] = $cabins;
			$allPrices[$hash] = $prices;

			$row = str_replace('|BGCOLOR|', '#'.substr($hash, 0, 3), $row);
		}
		else
			$row = str_replace('|BGCOLOR|', 'yellow', $row);

		echo $row; ?>
	</tr>
<?	} ?>
</table>


<p>Results:</p>
<?

$add = [];
$summary = [];

foreach(array_keys($allDates) as $i => $setting) {

	$tmp = [ // default setting config
		'pricing-method' => 'variable',
		'pricing-room-base' => 'enable',
		'template' => 0
	];

	$extra_date = [];
	$cabins = [];
	$lowestPrice = 0;
	

	$tpl_name = [];
	if(isset($_POST['per-cabin-pricing']))
		$tpl_name[] = 'CA';
	else
		$tpl_name[] = 'PP';
	
?>
<fieldset>
<legend>Setting # <?=($i+1)?></legend>
Cabins and prices:
<ul><?
	foreach($allCabins[$setting] as $j => $cabin) {
		if(isset($_POST['per-cabin-pricing']))
			$allPrices[$setting][$j] *= $cabin[1];

		if(!$lowestPrice || $allPrices[$setting][$j] < $lowestPrice)
			$lowestPrice = $allPrices[$setting][$j];

		$tpl_name[] = preg_replace('/\s*Cabin\s*/', '', $cabin[0]).' ('.$cabin[1].') '.$allPrices[$setting][$j];

		$cabins[] = ['name' => $cabin[0], 'price' => $allPrices[$setting][$j], 'spots' => $cabin[1], 'descr' => ''];
?>
	<li><?=$cabin[0]?> (€ <?=$allPrices[$setting][$j]?>) : <?=$cabin[1]?> spot(s)</li>
<?	} ?>
</ul>
Dates:
<table border="1">
	<tr><th>Departure</th><th>Return</th></tr>
<?	foreach($allDates[$setting] as $j => $dates) {
		$extra_date[] = $dates[0]; ?>
	<tr>
<?		foreach($dates as $date) { ?>
		<td><?=$date?></td>
<?		} ?>
	</tr>
<?	} ?>
</table>
</fieldset><br/>

<?

	$tpl_name = implode(' - ', $tpl_name);

	global $wpdb;
	$template = $wpdb->get_row('SELECT id, post_title FROM wp_posts WHERE post_status = "publish" AND post_type = "yacht_templates" AND post_title = "'.esc_sql($tpl_name).'" LIMIT 1');

	if(!$template) {

		$my_post = array(
			'post_title'    => $tpl_name,
			'post_type'    => 'yacht_templates',
			'post_status'   => 'publish',
			'post_author'   => get_current_user_id(),
			'meta_input' => array(
				'_yacht_cabins' => $cabins
			)
		);
		if(isset($_POST['per-cabin-pricing']))
			$my_post['meta_input']['_yacht_per_cabin_pricing'] = true;
	 
		$tmp['template'] = wp_insert_post($my_post);
		echo 'Using template = '.$tpl_name.' (ID = '. $tmp['template'] .')<br><br><br>';
		$summary['newtemplate'][] = $tpl_name;
	}
	else {
		$tmp['template'] = $template->id;
		echo 'Using template = '.$template->post_title.' (ID = '. $template->id .')<br><br><br>';
	}

	// derived values
	$tmp['num-rooms'] = count($cabins);
	$tmp['extra-date'] = implode(', ', $extra_date);
	$tmp['adult-price'] = $lowestPrice;
	$tmp['children-price'] = $lowestPrice;

	$add[] = $tmp; // queue for adding

}

echo '<pre>';

//var_dump($add);

$meta = get_post_meta($tour);
$meta['tourmaster-tour-option'][0] = maybe_unserialize($meta['tourmaster-tour-option'][0]);

$list_tour_date = explode(',', $meta['tourmaster-tour-date'][0]);
$list_tour_date_av = explode(',', $meta['tourmaster-tour-date-avail'][0]);

foreach($allDates as $dates) {
	foreach($dates as $date) {
		if( !in_array($date[0], $list_tour_date) ) {
			$list_tour_date[] = $date[0];
			$summary['newdate'][] = $date[0];
		}

		if( !in_array($date[0], $list_tour_date_av) )
			$list_tour_date_av[] = $date[0];

	}
}

$tour_ids = [$tour];
foreach(['de','en'] as $lang_code) {
	$id = icl_object_id($tour, 'tour', false, $lang_code);
	if($id != $tour)
		$tour_ids[] = $id;
}

if($update) {
	foreach($tour_ids as $tour_id) {
		update_post_meta($tour_id, 'tourmaster-tour-date', implode(',', $list_tour_date));
		update_post_meta($tour_id, 'tourmaster-tour-date-avail', implode(',', $list_tour_date_av));
	}
}

foreach($add as $setting) {
	// check if date already exists
	if(!empty($meta['tourmaster-tour-option'][0]['date-price'])) {
		foreach($meta['tourmaster-tour-option'][0]['date-price'] as $sid => $prevSetting) {
			if($prevSetting['extra-date'] === $setting['extra-date']) { // then apply only new values
				foreach($setting as $key => $newValue) {
					$meta['tourmaster-tour-option'][0]['date-price'][$sid][$key] = $newValue;
				}
				$summary['updatedtours'][] = $setting['extra-date'];
				continue 2;
			}
				
		}
	}

	// else, add a new setting
	$meta['tourmaster-tour-option'][0]['date-price'][] = $setting;
	$summary['newtours'][] = $setting['extra-date'];
}

if($update) {
	foreach($tour_ids as $tour_id) {
		update_post_meta($tour_id, 'tourmaster-tour-option', $meta['tourmaster-tour-option'][0]);
	}

	$tmp = ob_get_clean();

	foreach($summary as $type => $data) {
		switch($type) {
			case 'updatedtours': echo '<h1>Updated the following settings:</h1>'; break;
			case 'newtours': echo '<h1>Added the following settings:</h1>'; break;
			case 'newtemplate': echo '<h1>Created the following templates:</h1>'; break;
			case 'newdate': echo '<h1>Added the following tour dates:</h1>'; break;
			default: echo '<h1>UNKNOWN</h1>';
		}
		echo '<ol>';
		foreach($data as $row) {
			echo '<li style="margin: 12px 0;">'.$row.'</li>';
		}
		echo '</ol>';
	}

	echo $tmp;

/*foreach($meta as $k => $v) {
	if($k !== 'tourmaster-tour-option')
		continue;

	if(is_array($v)) {

		echo 'ARRAY '.$k.' => '.PHP_EOL;

		$val = $meta['tourmaster-tour-option'][0]['date-price'];
		echo var_export($val, 1).PHP_EOL;

	}
	else
		echo $k .' => '.var_export(maybe_unserialize($v), 1);
	echo PHP_EOL;
}*/
}

echo '</pre>';
?>
<label style="font-size: 16pt;"><input type="checkbox" name="per-cabin-pricing" value="1" <? if(isset($_POST['per-cabin-pricing'])) echo 'checked'; ?>>Per-cabin pricing?</labeL><br/>
<button class="big" name="addDates">Add/Update Dates</button>
</form>
</body>
</html>

<?
    exit();
}
